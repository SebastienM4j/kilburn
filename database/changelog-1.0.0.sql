--liquibase formatted sql

--changeset smo:1.0.0_001
CREATE TABLE operation_source (
	id BIGSERIAL NOT NULL,
	date_import TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    datasource VARCHAR NOT NULL,
    compte VARCHAR NOT NULL,
    data TEXT NOT NULL,
    CONSTRAINT pk_operation_source PRIMARY KEY (id)
);
--rollback DROP TABLE operation_source;

--changeset smo:1.0.0_002
CREATE TABLE compte (
	id SERIAL NOT NULL,
	type VARCHAR(10),
    libelle VARCHAR(100) NOT NULL,
    numero VARCHAR NOT NULL,
    iban VARCHAR(27),
    rib_banque VARCHAR(5),
    rib_guichet VARCHAR(5),
    rib_compte VARCHAR(11),
    rib_cle VARCHAR(2),
    bic VARCHAR(11),
    CONSTRAINT pk_compte PRIMARY KEY (id)
);
--rollback DROP TABLE compte;

--changeset smo:1.0.0_003
CREATE TABLE compte_tag (
    compte INT NOT NULL REFERENCES compte (id),
    tag VARCHAR(50) NOT NULL,
    CONSTRAINT pk_compte_tag PRIMARY KEY (compte, tag)
);
--rollback DROP TABLE compte_tag;

--changeset smo:1.0.0_004
CREATE TABLE operation (
	id BIGSERIAL NOT NULL,
	compte INT NOT NULL REFERENCES compte (id),
	source BIGINT REFERENCES operation_source (id),
	date TIMESTAMP WITHOUT TIME ZONE NOT NULL,
	date_valeur TIMESTAMP WITHOUT TIME ZONE,
	mode VARCHAR(11),
	tiers VARCHAR(70),
	libelle VARCHAR(100),
	reference VARCHAR(50),
	montant NUMERIC NOT NULL,
	commentaire TEXT,
	pointee BOOLEAN NOT NULL DEFAULT FALSE,
	CONSTRAINT pk_operation PRIMARY KEY (id)
);
--rollback DROP TABLE operation;

--changeset smo:1.0.0_005
CREATE TABLE operation_tag (
    operation BIGINT NOT NULL REFERENCES operation (id),
    tag VARCHAR(50) NOT NULL,
    CONSTRAINT pk_operation_tag PRIMARY KEY (operation, tag)
);
--rollback DROP TABLE operation_tag;

--changeset smo:1.0.0_006
ALTER TABLE operation ADD COLUMN provenance VARCHAR(10);
UPDATE operation SET provenance = 'IMPORT' WHERE provenance IS NULL;
ALTER TABLE operation ALTER COLUMN provenance SET NOT NULL;
--rollback ALTER TABLE operation DROP COLUMN provenance;

--changeset smo:1.0.0_007
ALTER TABLE operation ADD COLUMN type VARCHAR(10);
UPDATE operation SET type = 'ORIGINAL' WHERE type IS NULL;
ALTER TABLE operation ALTER COLUMN type SET NOT NULL;
--rollback ALTER TABLE operation DROP COLUMN type;

--changeset smo:1.0.0_008
CREATE TABLE operation_surcharge (
    operation BIGINT NOT NULL REFERENCES operation (id),
    operation_surchargee BIGINT NOT NULL REFERENCES operation (id),
    CONSTRAINT pk_operation_surcharge PRIMARY KEY (operation, operation_surchargee)
);
--rollback DROP TABLE operation_surcharge;

--changeset smo:1.0.0_009
CREATE TABLE operation_regle (
    id SERIAL NOT NULL,
    regle VARCHAR NOT NULL,
    libelle VARCHAR(100) NOT NULL,
    description TEXT,
    auto BOOLEAN NOT NULL DEFAULT FALSE,
    ordre INT NOT NULL,
    langage VARCHAR(10) NOT NULL,
    script TEXT NOT NULL,
    CONSTRAINT pk_operation_regle PRIMARY KEY (id)
);
--rollback DROP TABLE operation_regle;

--changeset smo:1.0.0_010
DELETE FROM operation_surcharge;
DROP TABLE operation_surcharge;
DELETE FROM operation_tag;
DELETE FROM operation;
ALTER SEQUENCE operation_id_seq RESTART WITH 1;

--changeset smo:1.0.0_011
CREATE TABLE operation_operation_source (
    operation BIGINT NOT NULL REFERENCES operation (id),
    operation_source BIGINT NOT NULL REFERENCES operation_source (id),
    CONSTRAINT pk_operation_operation_source PRIMARY KEY (operation, operation_source)
);
--rollback DROP TABLE operation_operation_source;

--changeset smo:1.0.0_012
ALTER TABLE operation DROP COLUMN source;

--changeset smo:1.0.0_013
ALTER TABLE operation_source ALTER COLUMN date_import TYPE timestamp with time zone;
--rollback ALTER TABLE operation_source ALTER COLUMN date_import TYPE timestamp without time zone;

--changeset smo:1.0.0_014
ALTER TABLE operation ALTER COLUMN date TYPE timestamp with time zone;
--rollback ALTER TABLE operation ALTER COLUMN date TYPE timestamp without time zone;

--changeset smo:1.0.0_015
ALTER TABLE operation ALTER COLUMN date_valeur TYPE timestamp with time zone;
--rollback ALTER TABLE operation ALTER COLUMN date_valeur TYPE timestamp without time zone;

--changeset smo:1.0.0_016
ALTER TABLE operation_source ADD CONSTRAINT unq_operation_source UNIQUE (datasource, compte, "data");
--rollback ALTER TABLE operation_source DROP CONSTRAINT unq_operation_source;
	