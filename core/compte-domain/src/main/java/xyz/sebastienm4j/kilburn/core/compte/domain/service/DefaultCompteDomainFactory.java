package xyz.sebastienm4j.kilburn.core.compte.domain.service;

import org.springframework.stereotype.Service;
import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;
import xyz.sebastienm4j.kilburn.api.compte.domain.factory.CompteDomainFactory;
import xyz.sebastienm4j.kilburn.core.compte.domain.entity.CompteEntity;

@Service
public class DefaultCompteDomainFactory implements CompteDomainFactory
{

    @Override
    public Compte newCompte()
    {
        return new CompteEntity();
    }

}
