package xyz.sebastienm4j.kilburn.core.compte.domain.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan("xyz.sebastienm4j.kilburn.core.compte.domain.entity")
@EnableJpaRepositories("xyz.sebastienm4j.kilburn.core.compte.domain.repository")
@ComponentScan("xyz.sebastienm4j.kilburn.core.compte.domain.service")
public class CoreCompteDomainSpringConfig
{

}
