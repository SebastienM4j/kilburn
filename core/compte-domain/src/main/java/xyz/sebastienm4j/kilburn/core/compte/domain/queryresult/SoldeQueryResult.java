package xyz.sebastienm4j.kilburn.core.compte.domain.queryresult;

import java.math.BigDecimal;

/**
 * Soldes d'un compte.
 */
public class SoldeQueryResult
{
    private BigDecimal pointe;
    private BigDecimal avenir;
    private BigDecimal solde;


    public SoldeQueryResult() {}

    public SoldeQueryResult(BigDecimal pointe, BigDecimal avenir)
    {
        this.pointe = pointe != null ? pointe : BigDecimal.ZERO;
        this.avenir = avenir != null ? avenir : BigDecimal.ZERO;
        this.solde = this.pointe.add(this.avenir);
    }


    public BigDecimal getPointe() {
        return pointe;
    }

    public void setPointe(BigDecimal pointe) {
        this.pointe = pointe;
    }

    public BigDecimal getAvenir() {
        return avenir;
    }

    public void setAvenir(BigDecimal avenir) {
        this.avenir = avenir;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

}
