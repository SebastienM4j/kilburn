package xyz.sebastienm4j.kilburn.core.compte.domain.queryresult;

import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;
import xyz.sebastienm4j.kilburn.core.compte.domain.entity.CompteEntity;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Compte bancaire avec son solde.
 */
public class CompteSoldeQueryResult implements Compte
{
    private Integer id;

    private Type type;
    private String libelle;

    private String numero;
    private String iban;

    private String ribBanque;
    private String ribGuichet;
    private String ribCompte;
    private String ribCle;

    private String bic;

    private Set<String> tags;

    private BigDecimal solde;


    public CompteSoldeQueryResult() {}

    public CompteSoldeQueryResult(CompteEntity cpt, BigDecimal solde)
    {
        if(cpt != null)
        {
            this.id = cpt.getId();
            this.type = cpt.getType();
            this.libelle = cpt.getLibelle();
            this.numero = cpt.getNumero();
            this.iban = cpt.getIban();
            this.ribBanque = cpt.getRibBanque();
            this.ribGuichet = cpt.getRibGuichet();
            this.ribCompte = cpt.getRibCompte();
            this.ribCle = cpt.getRibCle();
            this.bic = cpt.getBic();
            this.tags = cpt.getTags();
        }

        this.solde = solde;
        if(this.solde == null) {
            this.solde = BigDecimal.ZERO;
        }
    }


    @Override
    public Integer getId()
    {
        return id;
    }

    @Override
    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    @Enumerated(EnumType.STRING)
    public Type getType()
    {
        return type;
    }

    @Override
    public void setType(Type type)
    {
        this.type = type;
    }

    @Override
    public String getLibelle()
    {
        return libelle;
    }

    @Override
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    @Override
    public String getNumero()
    {
        return numero;
    }

    @Override
    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    @Override
    public String getIban()
    {
        return iban;
    }

    @Override
    public void setIban(String iban)
    {
        this.iban = iban;
    }

    @Override
    public String getRibBanque()
    {
        return ribBanque;
    }

    @Override
    public void setRibBanque(String ribBanque)
    {
        this.ribBanque = ribBanque;
    }

    @Override
    public String getRibGuichet()
    {
        return ribGuichet;
    }

    @Override
    public void setRibGuichet(String ribGuichet)
    {
        this.ribGuichet = ribGuichet;
    }

    @Override
    public String getRibCompte()
    {
        return ribCompte;
    }

    @Override
    public void setRibCompte(String ribCompte)
    {
        this.ribCompte = ribCompte;
    }

    @Override
    public String getRibCle()
    {
        return ribCle;
    }

    @Override
    public void setRibCle(String ribCle)
    {
        this.ribCle = ribCle;
    }

    @Override
    public String getBic()
    {
        return bic;
    }

    @Override
    public void setBic(String bic)
    {
        this.bic = bic;
    }

    @Override
    public Set<String> getTags()
    {
        return tags;
    }

    @Override
    public void setTags(Set<String> tags)
    {
        this.tags = tags;
    }

    public BigDecimal getSolde()
    {
        return solde;
    }

    public void setSolde(BigDecimal solde)
    {
        this.solde = solde;
    }

}
