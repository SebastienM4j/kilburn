package xyz.sebastienm4j.kilburn.core.operation.integration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.AmendOperationRegle;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.PackageOperationRegle;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationRegleEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationRegleRepository;
import xyz.sebastienm4j.kilburn.core.operation.domain.service.ApplicationOperationRegleService;

import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Service chargé d'appliquer les règles automatiques
 * lors de l'intégration des opérations.
 */
@Service
public class OperationRegleAutoService
{
    private static final Logger logger = LoggerFactory.getLogger(OperationRegleAutoService.class);


    @Autowired
    private OperationRegleRepository operationRegleRepository;

    @Autowired
    private ApplicationOperationRegleService applicationOperationRegleService;


    public List<Operation> applyRules(List<Operation> operations)
    {
        Assert.notEmpty(operations, "Liste des opérations null ou vide");


        // Conditionnement des opérations

        List<OperationRegleEntity> packageRules = operationRegleRepository.findByRegleAndAutoOrderByOrdreAsc(PackageOperationRegle.class.getName(), true);
        if(!isEmpty(packageRules))
        {
            for(OperationRegleEntity regle : packageRules)
            {
                try {
                    operations = applicationOperationRegleService.packaging(regle, operations);

                } catch(ScriptException ex) {
                    logger.error("Echec du conditionnement des opérations importées", ex);
                    return null;
                }
            }
        }

        // Modification des opérations

        List<Operation> result = new ArrayList<>(operations);

        List<OperationRegleEntity> amendRules = operationRegleRepository.findByRegleAndAutoOrderByOrdreAsc(AmendOperationRegle.class.getName(), true);
        if(!isEmpty(amendRules))
        {
            amendRules.forEach(r ->
            {
                try {
                    applicationOperationRegleService.amend(r, result);

                } catch(ScriptException ex) {
                    logger.error("Echec de l'exécution de la règle de modification d'opérations : id ["+r.getId()+"], libellé ["+r.getLibelle()+"] (script ["+r.getLangage().name()+"])", ex);
                }
            });
        }


        return result;
    }

}
