package xyz.sebastienm4j.kilburn.core.operation.integration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationTranslationException;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationTranslator;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Provenance;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Type;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;
import xyz.sebastienm4j.kilburn.core.compte.domain.entity.CompteEntity;
import xyz.sebastienm4j.kilburn.core.compte.domain.repository.CompteRepository;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationSourceEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationSourceRepository;

import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Service réalisant la traduction des opérations sources en opération.
 */
@Service
public class OperationTraductionService
{
    private static final Logger logger = LoggerFactory.getLogger(OperationTraductionService.class);


    @Autowired
    private List<OperationTranslator> translators;

    @Autowired
    private OperationSourceRepository operationSourceRepository;

    @Autowired
    private CompteRepository compteRepository;


    public List<Operation> translateOperations()
    {
        if(isEmpty(translators)) {
            logger.warn("Aucun traducteur d'opérations source n'est défini, les traductions ne peuvent pas se faire");
            return null;
        }


        // Récupération des opérations source n'ayant pas été transformées en opération

        List<OperationSourceEntity> operations = operationSourceRepository.findAllNotReferencedByOperation();
        if(isEmpty(operations)) {
            logger.debug("Aucune opération source n'est à traduire");
            return null;
        }

        logger.debug("[{}] opération(s) source sont à traduire", operations.size());

        // Pour chaque traducteur, extraire les opérations sources qu'il devra traduire (isApplicableTo)

        Map<OperationTranslator, List<OperationSourceEntity>> lots = translators.stream()
                                                                                .collect(Collectors.toMap(
                                                                                        t -> t,
                                                                                        t -> operations.stream()
                                                                                                       .filter(os -> t.isApplicableTo(os))
                                                                                                       .collect(Collectors.toList())));

        // Traduire les opérations en question

        List<CompteEntity> comptes = compteRepository.findAll();
        if(isEmpty(comptes)) {
            logger.warn("Aucun compte n'est défini, les traductions ne peuvent pas se faire");
            return null;
        }

        List<Operation> result = lots.entrySet().stream()
                                                .collect(ArrayList<Operation>::new,
                                                         (l, e) -> l.addAll(translateOperations(e.getKey(), e.getValue().stream().collect(Collectors.toList()), comptes.stream().collect(Collectors.toSet()))),
                                                         ArrayList<Operation>::addAll);

        logger.debug("[{}] opération(s) traduite(s) sur [{}] opération(s) source(s)", result.size(), operations.size());

        return result;
    }


    private List<Operation> translateOperations(OperationTranslator translator, List<OperationSource> operations, Set<Compte> comptes)
    {
        if(isEmpty(operations)) {
            return new ArrayList<>();
        }

        try {
            Operation op;
            List<Operation> result = new ArrayList<>();
            for(OperationSource os : operations)
            {
                op = translator.translate(os, comptes);

                // Vérifications
                if(op.getCompte() == null) {
                    logger.warn("Mauvaise traduction de l'opération source id [{}] (data [{}]) par le traducteur [{}] : absence du compte", os.getId(), os.getData(), translator.getClass().getName());
                    continue;
                }
                if(op.getMontant() == null) {
                    logger.warn("Mauvaise traduction de l'opération source id [{}] (data [{}]) par le traducteur [{}] : absence du montant", os.getId(), os.getData(), translator.getClass().getName());
                    continue;
                }

                // On met certaines valeurs par défaut
                op.setId(null);
                op.setPointee(false);
                op.setProvenance(Provenance.IMPORT);
                op.setType(Type.UNITAIRE);
                op.setSources(Arrays.asList(os));

                result.add(op);
            }

            logger.info("[{}] opération(s) traduite(s) par [{}]", result.size(), translator.getClass().getName());

            return result;

        } catch(OperationTranslationException ex) {
            logger.error("Echec de la traduction de certaines opérations source, traducteur ["+translator.getClass().getName()+"]", ex);
            return new ArrayList<>();
        }
    }

}
