package xyz.sebastienm4j.kilburn.core.operation.integration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationRepository;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Service rapprochant des opérations saisies manuellement à l'avance
 * avec des opérations en cours d'intégration.
 */
@Service
public class OperationManuelleRapprochementService
{
    private static final Logger logger = LoggerFactory.getLogger(OperationManuelleRapprochementService.class);


    @Autowired
    private OperationRepository operationRepository;


    public List<Operation> rapprochementOperations(List<Operation> operations)
    {
        Assert.notEmpty(operations, "Liste des opérations null ou vide, rapprochement impossible");


        // Recherche des opérations manuelles sans lien avec une opération source
        // et donc qui n'a pas été rapprochée avec une opération importée

        List<OperationEntity> candidates = operationRepository.findAllManellesNotLinkedWithSource();
        if(isEmpty(operations)) {
            return operations;
        }

        // Rapprochement (ou non) des opérations avec les opérations manuelles disponibles

        return operations.stream()
                         .map(o -> rapprochementOperation(o, candidates))
                         .collect(Collectors.toList());
    }


    private Operation rapprochementOperation(Operation operation, List<OperationEntity> candidates)
    {
        if(Operation.Mode.CHEQUE.equals(operation.getMode())) {
            return rapprochementCheque(operation, candidates);
        }

        return operation;
    }

    private Operation rapprochementCheque(Operation operation, List<OperationEntity> candidates)
    {
        if(!StringUtils.hasText(operation.getReference())) {
            return operation;
        }

        OperationEntity chequeManuel = candidates.stream()
                                                 .filter(c -> c.getCompte().getId() == operation.getCompte().getId())
                                                 .filter(c -> Operation.Mode.CHEQUE.equals(c.getMode()))
                                                 .filter(c -> StringUtils.hasText(c.getReference()))
                                                 .filter(c -> operation.getReference().equalsIgnoreCase(c.getReference()))
                                                 .filter(c -> operation.getMontant().compareTo(c.getMontant()) == 0)
                                                 .collect(Collectors.reducing((a, b) -> null))
                                                 .orElse(null);

        if(chequeManuel == null) {
            return operation;
        }

        logger.info("Rapprochement du chèque [{}] saisie manuellement [{}] avec la source [{}]", operation.getReference(), chequeManuel.getId(), operation.getSources().get(0).getId());

        chequeManuel.setDateValeur(chequeManuel.getDate());
        chequeManuel.setDate(operation.getDate());
        chequeManuel.setSources(operation.getSources());
        chequeManuel.setPointee(true);

        return chequeManuel;
    }

}
