package xyz.sebastienm4j.kilburn.core.operation.integration.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;
import xyz.sebastienm4j.kilburn.core.operation.domain.config.CoreOperationDomainSpringConfig;
import xyz.sebastienm4j.kilburn.core.operation.integration.config.CoreOperationIntegrationStandaloneProperties.Scheduling;
import xyz.sebastienm4j.kilburn.core.operation.integration.config.CoreOperationIntegrationStandaloneProperties.TaskExecutor;
import xyz.sebastienm4j.kilburn.core.operation.integration.service.OperationIntegrationService;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.springframework.util.StringUtils.hasText;

/**
 * Configuration Spring (autonome) pour le service d'intégration des opérations.
 */
@Configuration
@EnableScheduling
@EnableConfigurationProperties(CoreOperationIntegrationStandaloneProperties.class)
@ComponentScan("xyz.sebastienm4j.kilburn.core.operation.integration.service")
@Import(CoreOperationDomainSpringConfig.class)
public class CoreOperationIntegrationStandaloneSpringConfig implements SchedulingConfigurer
{
    private static final Logger logger = LoggerFactory.getLogger(CoreOperationIntegrationStandaloneSpringConfig.class);


    @Autowired
    private CoreOperationIntegrationStandaloneProperties coreOperationIntegrationProperties;

    @Autowired
    private OperationIntegrationService operationIntegrationService;


    @Bean(destroyMethod="shutdown")
    public Executor taskExecutor()
    {
        TaskExecutor taskExecutor = coreOperationIntegrationProperties.getTaskExecutor();
        if(taskExecutor == null) {
            taskExecutor = new TaskExecutor();
        }

        return Executors.newScheduledThreadPool(taskExecutor.getCorePoolSize());
    }


    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar)
    {
        taskRegistrar.setScheduler(taskExecutor());

        Trigger trigger = createTrigger(coreOperationIntegrationProperties.getScheduling());

        if(trigger == null) {
            logger.warn("Problème de configuration pour l'intégration des opérations (absence de scheduling), aucune intégration ne sera réalisée");
            return;
        }

        taskRegistrar.addTriggerTask(() -> operationIntegrationService.integrateOperations(), trigger);
    }

    private Trigger createTrigger(Scheduling scheduling)
    {
        if(scheduling == null) {
            return null;
        }

        if(hasText(scheduling.getCron())) {
            return new CronTrigger(scheduling.getCron());
        }

        if(scheduling.getPeriod() != null) {
            PeriodicTrigger trigger = new PeriodicTrigger(scheduling.getPeriod(), scheduling.getTimeUnit());
            trigger.setInitialDelay(scheduling.getInitialDelay());
            trigger.setFixedRate(scheduling.isFixedRate());
            return trigger;
        }

        return null;
    }
}
