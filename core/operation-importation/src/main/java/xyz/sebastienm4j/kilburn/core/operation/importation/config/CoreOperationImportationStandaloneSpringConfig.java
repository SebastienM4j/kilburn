package xyz.sebastienm4j.kilburn.core.operation.importation.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationDataSource;
import xyz.sebastienm4j.kilburn.core.operation.domain.config.CoreOperationDomainSpringConfig;
import xyz.sebastienm4j.kilburn.core.operation.importation.config.CoreOperationImportationStandaloneProperties.Scheduling;
import xyz.sebastienm4j.kilburn.core.operation.importation.config.CoreOperationImportationStandaloneProperties.TaskExecutor;
import xyz.sebastienm4j.kilburn.core.operation.importation.service.OperationImportationService;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.springframework.util.CollectionUtils.isEmpty;
import static org.springframework.util.StringUtils.hasText;

/**
 * Configuration Spring (autonome) pour le service d'importation d'opérations.
 */
@Configuration
@EnableScheduling
@EnableConfigurationProperties(CoreOperationImportationStandaloneProperties.class)
@ComponentScan("xyz.sebastienm4j.kilburn.core.operation.importation.service")
@Import(CoreOperationDomainSpringConfig.class)
public class CoreOperationImportationStandaloneSpringConfig implements SchedulingConfigurer
{
    private static final Logger logger = LoggerFactory.getLogger(CoreOperationImportationStandaloneSpringConfig.class);


    @Autowired
    private CoreOperationImportationStandaloneProperties coreOperationImportationProperties;

    @Autowired
    private OperationImportationService operationImportationService;

    @Autowired
    private List<OperationDataSource> dataSources;


    @Bean(destroyMethod="shutdown")
    public Executor taskExecutor()
    {
        TaskExecutor taskExecutor = coreOperationImportationProperties.getTaskExecutor();
        if(taskExecutor == null) {
            taskExecutor = new TaskExecutor();
        }

        return Executors.newScheduledThreadPool(taskExecutor.getCorePoolSize());
    }


    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar)
    {
        taskRegistrar.setScheduler(taskExecutor());


        if(isEmpty(coreOperationImportationProperties.getImports())) {
            logger.warn("Aucun import d'opérations n'est configuré");
            return;
        }


        coreOperationImportationProperties.getImports().forEach(i ->
        {
            Trigger trigger = createTrigger(i.getScheduling());

            if(trigger == null || !hasText(i.getDatasourceClassName())) {
                logger.warn("Problème de configuration d'un import (absence de scheduling et/ou datasourceClassName), l'import est ignoré");
                return;
            }

            OperationDataSource datasource = dataSources.stream()
                                                        .filter(ds -> ds.getClass().getName().equals(i.getDatasourceClassName()))
                                                        .findFirst()
                                                        .orElse(null);
            if(datasource == null) {
                logger.warn("La source de données [{}] n'est pas chargée, l'import est ignoré", i.getDatasourceClassName());
                return;
            }

            taskRegistrar.addTriggerTask(() -> operationImportationService.importOperationsFrom(datasource, i.getDatasourceProperties()), trigger);
        });
    }

    private Trigger createTrigger(Scheduling scheduling)
    {
        if(scheduling == null) {
            return null;
        }

        if(hasText(scheduling.getCron())) {
            return new CronTrigger(scheduling.getCron());
        }

        if(scheduling.getPeriod() != null) {
            PeriodicTrigger trigger = new PeriodicTrigger(scheduling.getPeriod(), scheduling.getTimeUnit());
            trigger.setInitialDelay(scheduling.getInitialDelay());
            trigger.setFixedRate(scheduling.isFixedRate());
            return trigger;
        }

        return null;
    }
}
