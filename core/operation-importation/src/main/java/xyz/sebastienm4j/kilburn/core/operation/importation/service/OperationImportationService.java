package xyz.sebastienm4j.kilburn.core.operation.importation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationDataSource;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationDataSourceException;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationSourceEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationSourceRepository;

import java.time.Instant;
import java.util.Collection;
import java.util.Map;

import static org.springframework.util.StringUtils.hasText;

/**
 * Service réalisant l'importation d'opération à partir d'une source de données.
 */
@Service
public class OperationImportationService
{
    private static final Logger logger = LoggerFactory.getLogger(OperationImportationService.class);

    @Autowired
    private OperationSourceRepository operationSourceRepository;


    public void importOperationsFrom(OperationDataSource dataSource, Map<String, String> dataSourceProperties)
    {
        Assert.notNull(dataSource, "Absence de source de données");

        logger.debug("Importation des opérations depuis la source de données [{}]", dataSource.getClass().getName());

        try {
            Collection<OperationSource> operations = dataSource.getLastOperations(dataSourceProperties);

            long imported = operations.stream()
                                      .filter(o -> importOperation(dataSource, o))
                                      .count();

            logger.info("[{}] opération(s) importée(s) depuis la source de données [{}]", imported, dataSource.getClass().getName());

        } catch(OperationDataSourceException ex) {
            logger.error("Echec de l'importation des opérations depuis la source de données ["+dataSource.getClass().getName()+"]", ex);
        }
    }


    private boolean importOperation(OperationDataSource dataSource, OperationSource operation)
    {
        if(!hasText(operation.getDatasource())) {
            logger.warn("Absence d'identifiant de la source de données, l'opération [{}] n'est pas importée", operation.getData());
            return false;
        }
        if(!hasText(operation.getCompte())) {
            logger.warn("Absence d'identifiant de compte (rib, iban ou numéro), l'opération [{}] n'est pas importée", operation.getData());
            return false;
        }
        if(!hasText(operation.getData())) {
            logger.warn("Absence de données, l'opération n'est pas importée");
            return false;
        }

        operation.setId(null);
        if(operation.getDateImport() == null) {
            operation.setDateImport(Instant.now());
        }

        if(operationSourceRepository.countByDatasourceAndCompteAndData(operation.getDatasource(), operation.getCompte(), operation.getData()) == 0)
        {
            logger.debug("Import de l'opération [{}] du compte [{}] depuis la source de données [{}]", operation.getData(), operation.getCompte(), dataSource.getClass().getName());

            try {
                operationSourceRepository.save((OperationSourceEntity)operation);
                return true;

            } catch(Exception ex) {
                logger.error("Echec de l'importation de l'opération ["+operation.getData()+"] du compte ["+operation.getCompte()+"] depuis la source données ["+dataSource.getClass().getName()+"]", ex);
            }
        }

        return false;
    }

}
