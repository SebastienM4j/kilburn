package xyz.sebastienm4j.kilburn.core.operation.domain.bean;

/**
 * Contient l'information sur la validité d'un script.
 */
public class ScriptValidation
{
    private boolean valid;
    private String error;


    public ScriptValidation()
    {
        this.valid = true;
    }

    public ScriptValidation(String error)
    {
        this.valid = false;
        this.error = error;
    }


    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
