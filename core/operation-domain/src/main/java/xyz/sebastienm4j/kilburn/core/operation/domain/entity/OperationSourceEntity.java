package xyz.sebastienm4j.kilburn.core.operation.domain.entity;

import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name="operation_source")
public class OperationSourceEntity implements OperationSource
{
    private Long id;
    private Instant dateImport;
    private String datasource;
    private String compte;
    private String data;


    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", nullable=false)
    @Override
    public Long getId()
    {
        return id;
    }

    @Override
    public void setId(Long id)
    {
        this.id = id;
    }

    @Override
    @Column(name="date_import", nullable=false)
    public Instant getDateImport()
    {
        return dateImport;
    }

    @Override
    public void setDateImport(Instant dateImport)
    {
        this.dateImport = dateImport;
    }

    @Override
    @Column(name="datasource", nullable=false)
    public String getDatasource()
    {
        return datasource;
    }

    @Override
    public void setDatasource(String datasource)
    {
        this.datasource = datasource;
    }

    @Override
    @Column(name="compte", nullable=false)
    public String getCompte()
    {
        return compte;
    }

    @Override
    public void setCompte(String compte)
    {
        this.compte = compte;
    }

    @Override
    @Column(name="data", nullable=false)
    public String getData()
    {
        return data;
    }

    @Override
    public void setData(String data)
    {
        this.data = data;
    }

}
