package xyz.sebastienm4j.kilburn.core.operation.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationRegleEntity;

import java.util.List;

@Repository
public interface OperationRegleRepository extends JpaRepository<OperationRegleEntity, Integer>
{

    Long countByRegle(String regle);

    List<OperationRegleEntity> findByRegleOrderByOrdreAsc(String regle);

    List<OperationRegleEntity> findByRegleAndAutoOrderByOrdreAsc(String regle, boolean auto);

    @Modifying
    @Query("UPDATE OperationRegleEntity r SET r.ordre = r.ordre+1 WHERE r.ordre >= ?1 AND r.ordre <= ?2")
    void incrementOrdreRegles(int ordreMin, int ordreMax);

    @Modifying
    @Query("UPDATE OperationRegleEntity r SET r.ordre = r.ordre-1 WHERE r.ordre >= ?1 AND r.ordre <= ?2")
    void decrementOrdreRegles(int ordreMin, int ordreMax);

}
