package xyz.sebastienm4j.kilburn.core.operation.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Provenance;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Type;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationEntity;

import java.util.Collection;
import java.util.List;

@Repository
public interface OperationRepository extends JpaRepository<OperationEntity, Long>
{

    List<OperationEntity> findByCompteIdOrderByDateDesc(Integer idCompte);

    List<OperationEntity> findByProvenanceAndTypeOrderByDate(Provenance provenance, Type type);

    OperationEntity findByCompteIdAndId(Integer idCompte, Long id);

    void deleteByCompteId(Integer idCompte);

    List<OperationEntity> findByIdIn(Collection<Long> ids);

    @Query("SELECT o FROM OperationEntity o WHERE o.provenance = 'MANUELLE' AND o.sources IS EMPTY")
    List<OperationEntity> findAllManellesNotLinkedWithSource();

}
