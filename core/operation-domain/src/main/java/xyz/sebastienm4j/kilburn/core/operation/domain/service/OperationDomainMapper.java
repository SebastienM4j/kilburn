package xyz.sebastienm4j.kilburn.core.operation.domain.service;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.AmendOperation;
import xyz.sebastienm4j.kilburn.core.compte.domain.entity.CompteEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationSourceEntity;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Permet de mapper des objets vers d'autres.
 */
@Component
public class OperationDomainMapper extends ConfigurableMapper
{

    @Override
    protected void configure(MapperFactory factory)
    {
        // FIXME à supprimer quand orika sera mise à jour avec ceci https://github.com/orika-mapper/orika/pull/178
        ConverterFactory converterFactory = factory.getConverterFactory();
        converterFactory.registerConverter(new PassThroughConverter(LocalDate.class));
        converterFactory.registerConverter(new PassThroughConverter(LocalDateTime.class));
        converterFactory.registerConverter(new PassThroughConverter(Instant.class));

        factory.registerConcreteType(Compte.class, CompteEntity.class);
        factory.registerConcreteType(OperationSource.class, OperationSourceEntity.class);

        factory.classMap(Operation.class, AmendOperation.class)
               .byDefault();
    }

}
