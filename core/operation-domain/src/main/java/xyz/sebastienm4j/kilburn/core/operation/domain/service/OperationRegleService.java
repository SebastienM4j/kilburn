package xyz.sebastienm4j.kilburn.core.operation.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import xyz.sebastienm4j.kilburn.api.compte.domain.factory.CompteDomainFactory;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Mode;
import xyz.sebastienm4j.kilburn.api.operation.domain.factory.OperationDomainFactory;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.AmendOperation;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.AmendOperationRegle;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.OperationRegleRegistry;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.PackageOperationRegle;
import xyz.sebastienm4j.kilburn.core.operation.domain.bean.ScriptValidation;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationRegleEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationRegleEntity.Langage;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationRegleRepository;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationRepository;

import javax.annotation.PostConstruct;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

@Service
public class OperationRegleService
{
    private static final Logger logger = LoggerFactory.getLogger(OperationRegleService.class);


    @Autowired
    private OperationRegleRepository operationRegleRepository;

    private ScriptEngineManager scriptEngineManager;

    @PostConstruct
    public void initScriptEngineManager()
    {
        scriptEngineManager = new ScriptEngineManager();
    }

    @Autowired
    private CompteDomainFactory compteDomainFactory;

    @Autowired
    private OperationDomainFactory operationDomainFactory;

    @Autowired
    private ApplicationOperationRegleService applicationOperationRegleService;

    @Autowired
    private OperationRepository operationRepository;


    @Transactional
    public void reorderRegle(OperationRegleEntity regle, int position)
    {
        if(position < regle.getOrdre()) {
            operationRegleRepository.incrementOrdreRegles(position, regle.getOrdre()-1);
        } else {
            operationRegleRepository.decrementOrdreRegles(regle.getOrdre()+1, position);
        }

        regle.setOrdre(position);
        operationRegleRepository.save(regle);
    }

    public ScriptValidation checkScript(OperationRegleRegistry type, Langage langage, String script)
    {
        Assert.notNull(type, "Absence du type");
        Assert.notNull(langage, "Absence du langage");
        Assert.hasText(script, "Absence du script");

        try {
            ScriptEngine engine = scriptEngineManager.getEngineByName(langage.name().toLowerCase());
            engine.eval(script);
            Invocable invocable = (Invocable)engine;

            switch(type)
            {
                case AMEND:
                    AmendOperationRegle amendRule = invocable.getInterface(AmendOperationRegle.class);
                    if(amendRule != null)
                    {
                        AmendOperation op = new AmendOperation();
                        op.setId(1L);
                        op.setDate(Instant.now());
                        op.setCompte(compteDomainFactory.newCompte());
                        op.getCompte().setId(1);
                        op.getCompte().setLibelle("Compte de test");
                        op.getCompte().setNumero("123456789");
                        op.setLibelle("CARTE Test Amend");
                        op.setMode(Mode.CARTE);
                        op.setMontant(BigDecimal.valueOf(-27.59));

                        amendRule.amend(op);

                        return new ScriptValidation();
                    }
                    break;

                case PACKAGE:
                    PackageOperationRegle packageRule = invocable.getInterface(PackageOperationRegle.class);
                    if(packageRule != null)
                    {
                        List<Operation> ops = new ArrayList<>();

                        Operation o1 = operationDomainFactory.newOperation();
                        o1.setId(1L);
                        o1.setDate(Instant.now());
                        o1.setCompte(compteDomainFactory.newCompte());
                        o1.getCompte().setId(1);
                        o1.getCompte().setLibelle("Compte de test");
                        o1.getCompte().setNumero("123456789");
                        o1.setLibelle("CARTE Test Package");
                        o1.setMode(Mode.CARTE);
                        o1.setMontant(BigDecimal.valueOf(-27.59));
                        ops.add(o1);

                        Operation o2 = operationDomainFactory.newOperation();
                        o2.setId(2L);
                        o2.setDate(Instant.now());
                        o2.setCompte(o1.getCompte());
                        o2.setLibelle("PRLV Test Package");
                        o2.setMode(Mode.PRELEVEMENT);
                        o2.setMontant(BigDecimal.valueOf(-19.09));
                        ops.add(o2);

                        packageRule.packaging(ops);

                        return new ScriptValidation();
                    }
                    break;
            }

            return new ScriptValidation("Erreur inconnue");

        } catch(Exception ex) {
            logger.warn("Erreur lors de la validation du script ["+langage.name()+"] (règle : ["+type.getRegleType().getName()+"]) ["+script+"]", ex);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);

            return new ScriptValidation(sw.toString());
        }
    }

    @Transactional(rollbackFor=ScriptException.class)
    public void applyRegleAndSave(OperationRegleEntity regle, List<Operation> operations) throws ScriptException
    {
        Assert.notNull(regle, "Absence de la règle");
        Assert.notEmpty(operations, "Liste des opérations null ou vide");

        List<Operation> operationsTransformed = null;
        OperationRegleRegistry type = OperationRegleRegistry.getByType(regle.getRegle());
        switch(type)
        {
            case PACKAGE:
                operationsTransformed = applicationOperationRegleService.packaging(regle, operations);
                break;

            case AMEND:
                operationsTransformed = applicationOperationRegleService.amend(regle, operations);
                break;
        }

        if(isEmpty(operationsTransformed)) {
            return;
        }

        operationRepository.save(operationsTransformed.stream().map(o -> (OperationEntity)o).collect(Collectors.toList()));
    }

}
