package xyz.sebastienm4j.kilburn.shared.rest.client;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedList;
import java.util.List;

import static org.springframework.util.StringUtils.hasText;

/**
 * Classe de base pour les services appelant des WS REST.
 */
public abstract class RestClientService
{
    @Value("${rest.timeout.connect:#{5000}}")
    private Integer connectTimeout;

    @Value("${rest.timeout.read:#{60000}}")
    private Integer readTimeout;

    @Value("${rest.proxy.host:#{null}}")
    private String proxyHost;

    @Value("${rest.proxy.port:#{null}}")
    private Integer proxyPort;

    @Value("${rest.proxy.username:#{null}}")
    private String proxyUsername;

    @Value("${rest.proxy.password:#{null}}")
    private String proxyPassword;


    /**
     * Configure et retourne un client REST.
     *
     * <p>Option possibles (configuration externalisée, cf http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html) :
     * <pre>
     *  - rest.timeout.connect : Timeout de connexion (défaut 5000ms)
     *  - rest.timeout.read : Timeout de lecture (défaut 60000ms)
     *  - http.proxy.host : IP du proxy
     *  - http.proxy.port : Port du proxy
     *  - http.proxy.username : Login pour l'authentification sur le proxy
     *  - http.proxy.password : Mot de passe pour l'authentification sur le proxy
     * </pre>
     *
     * @param expectedStatus Codes de retour attendus (non <tt>null</tt>, non vide)
     * @return Un client REST configuré
     * @see RestResponseHandler
     */
    public RestTemplate restTemplate(HttpStatus... expectedStatus)
    {
        Assert.notEmpty(expectedStatus, "Liste des status attendus null ou vide");

        HttpClientBuilder httpClientBuilder = httpClientBuilder();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClientBuilder.build());

        RestTemplate template = new RestTemplate(factory);
        template.setErrorHandler(new RestResponseHandler(expectedStatus));

        return template;
    }

    /*
     * Configure par défaut un HttpClient avec les options de timeout,
     * user-agent et proxy à utiliser pour instancier un RestTamplate.
     */
    private HttpClientBuilder httpClientBuilder()
    {
        HttpClientBuilder httpClientBuilder = HttpClients.custom();


        // Configuration des timeout
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(connectTimeout)
                .setConnectionRequestTimeout(connectTimeout)
                .setSocketTimeout(readTimeout)
                .build();

        SocketConfig socketConfig = SocketConfig.custom().setSoTimeout(readTimeout).build();

        httpClientBuilder.setDefaultRequestConfig(requestConfig);
        httpClientBuilder.setDefaultSocketConfig(socketConfig);


        // User agent
        List<Header> headers = new LinkedList<>();
        headers.add(new BasicHeader(HttpHeaders.USER_AGENT, "Kilburn"));

        httpClientBuilder.setDefaultHeaders(headers);


        // Proxy
        if(hasText(proxyHost) && proxyPort != null)
        {
            HttpHost proxy = new HttpHost(proxyHost, proxyPort);
            httpClientBuilder.setProxy(proxy);

            if(hasText(proxyUsername) && hasText(proxyPassword))
            {
                CredentialsProvider authProvider = new BasicCredentialsProvider();
                authProvider.setCredentials(new AuthScope(proxy), new UsernamePasswordCredentials(proxyUsername, proxyPassword));
                httpClientBuilder.setDefaultCredentialsProvider(authProvider);
            }
        }

        return httpClientBuilder;
    }


    /**
     * Fournis l'URL de base de l'API à interroger.
     *
     * <p>Par exemple pour une application déployée
     * sur le serveur <tt>http://localhost:8080</tt>
     * si on veut interroger la ressource <tt>/exemples/{code}</tt>
     * pour l'API v1 mappée sur <tt>/rest/api/v1</tt>, cette méthode
     * doit retourner <tt>http://localhost:8080/rest/api/v1<tt> et
     * la métode {@link #url(String)} sera appelée avec <tt>/exemples/{code}</tt>.
     *
     * <p>Ne pas utiliser cette méthode directement, mais bien passer par les
     * méthodes {@link #url()} et {@link #url(RestTemplate, String, Object...)}.
     *
     * @return L'URL de base de l'API à interroger
     */
    public abstract String baseUrl();

    /**
     * Construit l'URL complète à interroger à partir
     * de la base de l'URL ({@link #baseUrl()}.
     *
     * @return L'UL à interroger
     * @see #baseUrl()
     */
    public String url()
    {
        return baseUrl();
    }

    /**
     * Construit l'URL complète à interroger à partir
     * de la base de l'URL ({@link #baseUrl()} et de la
     * fin de l'URL, sans paramètre.
     *
     * @param resourcePath Fin de l'URL (non <tt>null</tt>, non vide)
     * @return L'URL à interroger
     * @see #baseUrl()
     */
    public String url(String resourcePath)
    {
        Assert.hasText(resourcePath, "Absence d'URL ou URL vide");

        return baseUrl()+resourcePath;
    }

    /**
     * Construit l'URL complète à interroger à partir
     * de la base de l'URL ({@link #baseUrl()} et de la
     * fin de l'URL, c'est à dire la resource avec ces
     * paramètres.
     *
     * @param template Template nécessaire à la construction de l'URL en incluant les variables (non <tt>null</tt>)
     * @param resourcePath Fin de l'URL (non <tt>null</tt>, non vide)
     * @param variables Variables de l'URL
     * @return L'URL à interroger
     * @see #baseUrl()
     */
    public String url(RestTemplate template, String resourcePath, Object... variables)
    {
        Assert.notNull(template, "Absence du RestTemplate");
        Assert.hasText(resourcePath, "Absence d'URL ou URL vide");

        String url = template.getUriTemplateHandler().expand(baseUrl()+resourcePath, variables).toString();

        return url;
    }

}
