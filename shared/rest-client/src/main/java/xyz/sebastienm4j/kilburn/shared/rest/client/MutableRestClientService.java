package xyz.sebastienm4j.kilburn.shared.rest.client;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Client REST dont l'URL de base ({@link #baseUrl()} peut être modifiée.
 */
@Service
@Scope("prototype")
public class MutableRestClientService extends RestClientService
{
    private String baseUrl;

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Override
    public String baseUrl()
    {
        return baseUrl;
    }

}
