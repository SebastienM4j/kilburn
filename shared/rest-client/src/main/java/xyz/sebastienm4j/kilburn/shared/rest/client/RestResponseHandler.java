package xyz.sebastienm4j.kilburn.shared.rest.client;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatus.Series;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.Assert;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * {@link ResponseErrorHandler} pour les appels REST permettant
 * de définir les code attendus. Dans le cas contraire, une
 * exception {@link RestClientUnexpectedResponseException} est levée
 * avec un message pouvant être définit ({@link #getMessage(HttpStatus, HttpHeaders)}).
 */
public class RestResponseHandler implements ResponseErrorHandler
{
    protected List<HttpStatus> expectedStatus;
    protected List<Series> expectedSeries;


    /**
     * Instancie le {@link ResponseErrorHandler} en spécifiant les codes
     * de retour attendus. Tous les autres provoqueront l'exception {@link RestClientUnexpectedResponseException}.
     *
     * @param expectedStatus Codes de retour attendus (non <tt>null</tt>, non vide)
     */
    public RestResponseHandler(HttpStatus... expectedStatus)
    {
        Assert.notEmpty(expectedStatus, "Liste des status attendus null ou vide");

        this.expectedStatus = Arrays.asList(expectedStatus);
        this.expectedSeries = new ArrayList<>();
    }

    /**
     * Instancie le {@link ResponseErrorHandler} en spécifiant les séries de codes
     * de retour attendues. Tous les autres code de retours provoqueront l'exception
     * {@link RestClientUnexpectedResponseException}.
     *
     * @param expectedSeries Séries de codes de retour attendues (non <tt>null</tt>, non vide)
     */
    public RestResponseHandler(Series... expectedSeries)
    {
        Assert.notEmpty(expectedSeries, "Liste des séries de status attendus null ou vide");

        this.expectedStatus = new ArrayList<>();
        this.expectedSeries = Arrays.asList(expectedSeries);
    }

    /**
     * Instancie le {@link ResponseErrorHandler} en spécifiant les codes ou les séries de codes
     * de retour attendus. Tous les autres code de retours provoqueront l'exception
     * {@link RestClientUnexpectedResponseException}.
     *
     * @param expectedStatus Code de retour attendus (non <tt>null</tt>, non vide)
     * @param expectedSeries Séries de codes de retour attendues (non <tt>null</tt>, non vide)
     */
    public RestResponseHandler(List<HttpStatus> expectedStatus, List<Series>  expectedSeries)
    {
        Assert.notEmpty(expectedStatus, "Liste des status attendus null ou vide");
        Assert.notEmpty(expectedSeries, "Liste des séries de status attendus null ou vide");

        this.expectedStatus = expectedStatus;
        this.expectedSeries = expectedSeries;
    }



    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException
    {
        return !expectedStatus.contains(response.getStatusCode()) &&
               !expectedSeries.contains(Series.valueOf(response.getStatusCode()));
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException
    {
        throw new RestClientUnexpectedResponseException(response.getStatusCode(), getMessage(response.getStatusCode(), response.getHeaders()));
    }

    /**
     * Fournis un message vis-à-vis du code de retour et éventuelement des entêtes de la réponse.
     *
     * @param statusCode Code HTTP suite à l'appel REST (non <tt>null</tt>)
     * @param headers Entêtes de la réponse
     * @return Le message (par défaut il s'agit du message standard)
     */
    public String getMessage(HttpStatus statusCode, HttpHeaders headers)
    {
        Assert.notNull(statusCode, "Absence du code de status");

        return statusCode.getReasonPhrase();
    }

}
