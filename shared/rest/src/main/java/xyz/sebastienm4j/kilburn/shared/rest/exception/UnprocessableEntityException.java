package xyz.sebastienm4j.kilburn.shared.rest.exception;

public class UnprocessableEntityException extends RuntimeException
{
    private static final long serialVersionUID = 8640081611827212457L;


    public UnprocessableEntityException() {
        super();
    }

    public UnprocessableEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnprocessableEntityException(String message) {
        super(message);
    }

    public UnprocessableEntityException(Throwable cause) {
        super(cause);
    }

}
