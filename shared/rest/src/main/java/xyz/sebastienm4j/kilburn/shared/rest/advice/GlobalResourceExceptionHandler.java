package xyz.sebastienm4j.kilburn.shared.rest.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import xyz.sebastienm4j.kilburn.shared.rest.exception.NotFoundException;
import xyz.sebastienm4j.kilburn.shared.rest.exception.UnprocessableEntityException;

@ControllerAdvice
public class GlobalResourceExceptionHandler
{

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleNotFound() {}

    @ExceptionHandler(UnprocessableEntityException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public void handleUnprocessableEntity() {}

}
