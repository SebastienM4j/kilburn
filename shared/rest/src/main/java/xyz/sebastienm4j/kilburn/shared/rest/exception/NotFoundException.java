package xyz.sebastienm4j.kilburn.shared.rest.exception;

public class NotFoundException extends RuntimeException
{
    private static final long serialVersionUID = -2333004890001756246L;


    public NotFoundException() {
        super();
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }

}
