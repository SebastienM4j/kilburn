package xyz.sebastienm4j.kilburn.shared.jpa.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan("xyz.sebastienm4j.kilburn.shared.jpa.converter")
public class JpaSpringConfig
{

}
