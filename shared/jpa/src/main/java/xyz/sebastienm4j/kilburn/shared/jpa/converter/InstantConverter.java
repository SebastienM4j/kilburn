package xyz.sebastienm4j.kilburn.shared.jpa.converter;

import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.Instant;

@Component
@Converter(autoApply=true)
public class InstantConverter implements AttributeConverter<Instant, Timestamp>
{

    @Override
    public Timestamp convertToDatabaseColumn(Instant attribute)
    {
        return attribute != null ? Timestamp.from(attribute) : null;
    }

    @Override
    public Instant convertToEntityAttribute(Timestamp dbData)
    {
        return dbData != null ? dbData.toInstant() : null;
    }

}
