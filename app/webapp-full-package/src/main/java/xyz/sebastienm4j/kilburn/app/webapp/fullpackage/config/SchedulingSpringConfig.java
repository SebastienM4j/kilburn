package xyz.sebastienm4j.kilburn.app.webapp.fullpackage.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import xyz.sebastienm4j.kilburn.app.webapp.fullpackage.config.SchedulingProperties.TaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
@EnableConfigurationProperties(SchedulingProperties.class)
public class SchedulingSpringConfig
{
    @Autowired
    private SchedulingProperties schedulingProperties;


    @Bean(destroyMethod="shutdown")
    public Executor taskExecutor()
    {
        TaskExecutor taskExecutor = schedulingProperties.getTaskExecutor();
        if(taskExecutor == null) {
            taskExecutor = new TaskExecutor();
        }

        return Executors.newScheduledThreadPool(taskExecutor.getCorePoolSize());
    }

}
