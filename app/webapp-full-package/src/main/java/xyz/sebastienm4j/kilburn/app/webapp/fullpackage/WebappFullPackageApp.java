package xyz.sebastienm4j.kilburn.app.webapp.fullpackage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import xyz.sebastienm4j.kilburn.app.webapp.fullpackage.config.SchedulingSpringConfig;
import xyz.sebastienm4j.kilburn.backend.compte.config.BackendCompteSpringConfig;
import xyz.sebastienm4j.kilburn.backend.operation.config.BackendOperationSpringConfig;
import xyz.sebastienm4j.kilburn.core.operation.importation.config.CoreOperationImportationSpringConfig;
import xyz.sebastienm4j.kilburn.core.operation.integration.config.CoreOperationIntegrationSpringConfig;
import xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.config.SupportOperationDataSourceMonCmbSpringConfig;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@Import({
	BackendCompteSpringConfig.class,
	BackendOperationSpringConfig.class,

	SchedulingSpringConfig.class,
	CoreOperationImportationSpringConfig.class,
	CoreOperationIntegrationSpringConfig.class,
    SupportOperationDataSourceMonCmbSpringConfig.class
})
public class WebappFullPackageApp extends SpringBootServletInitializer
{
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
    {
        builder.sources(WebappFullPackageApp.class)
               .application().setDefaultProperties(defaultProperties());
        return builder;
    }

	public static void main(String[] args)
    {
        SpringApplication application = new SpringApplication(WebappFullPackageApp.class);
        application.setDefaultProperties(defaultProperties());
        application.run(args);
    }


    /**
     * @return Les propriétés par défaut de la configuration Spring Boot
     */
    private static Map<String, Object> defaultProperties()
    {
        Map<String, Object> defaultProperties = new HashMap<>();
        defaultProperties.put("spring.profiles.active", "dev");
        defaultProperties.put("server.port", "12180");
        return defaultProperties;
    }

}
