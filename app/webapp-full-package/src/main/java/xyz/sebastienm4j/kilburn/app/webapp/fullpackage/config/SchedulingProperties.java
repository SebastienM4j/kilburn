package xyz.sebastienm4j.kilburn.app.webapp.fullpackage.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="scheduling")
public class SchedulingProperties
{
    public static class TaskExecutor
    {
        private int corePoolSize = 5;

        public int getCorePoolSize() {
            return corePoolSize;
        }

        public void setCorePoolSize(int corePoolSize) {
            this.corePoolSize = corePoolSize;
        }
    }


    private TaskExecutor taskExecutor;


    public TaskExecutor getTaskExecutor() {
        return taskExecutor;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

}
