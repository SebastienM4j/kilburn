package xyz.sebastienm4j.kilburn.app.operation.importation.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import xyz.sebastienm4j.kilburn.core.operation.importation.config.CoreOperationImportationStandaloneSpringConfig;
import xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.config.SupportOperationDataSourceMonCmbSpringConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * Point d'entrée du batch d'importation des opérations.
 */
@SpringBootApplication
@Import({
    CoreOperationImportationStandaloneSpringConfig.class,
    SupportOperationDataSourceMonCmbSpringConfig.class
})
public class OperationImportationBatchApp
{

    public static void main(String[] args)
    {
        SpringApplication application = new SpringApplication(OperationImportationBatchApp.class);
        application.setDefaultProperties(defaultProperties());
        application.run(args);
    }


    /**
     * @return Les propriétés par défaut de la configuration Spring Boot
     */
    private static Map<String, Object> defaultProperties()
    {
        Map<String, Object> defaultProperties = new HashMap<>();
        defaultProperties.put("spring.profiles.active", "dev");
        return defaultProperties;
    }

}
