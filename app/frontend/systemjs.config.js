/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
	System.config(
	{
		paths: {
	      // paths serve as alias
	      'npm:': 'node_modules/'
		},
		
		// map tells the System loader where to look for things
		map: {
			// our app is within the app folder
			app: 'app/js/app',

			// angular bundles
			'@angular/core': 'npm:@angular/core/bundles/core.umd.js',
			'@angular/common': 'npm:@angular/common/bundles/common.umd.js',
			'@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
			'@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
			'@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
			'@angular/http': 'npm:@angular/http/bundles/http.umd.js',
			'@angular/router': 'npm:@angular/router/bundles/router.umd.js',
			'@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
	
			// other libraries
			'rxjs': 'npm:rxjs',
			'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
	      
			'@ng-bootstrap/ng-bootstrap': 'npm:@ng-bootstrap/ng-bootstrap/bundles/ng-bootstrap.js',
			'angular2-click-outside': 'npm:angular2-click-outside/clickOutside.directive.js',
			'angular2-contextmenu': 'npm:angular2-contextmenu', // FIXME passer à ngx-contextmenu
			
			'ng2-ace-editor': 'npm:ng2-ace-editor',
			'ace-builds': 'npm:ace-builds/src-min/ace.js',
			'brace': 'npm:brace',
			'buffer': 'npm:buffers',
			'w3c-blob': 'npm:w3c-blob',
			
			'moment': 'npm:moment/min/moment-with-locales.min.js',
			'decimal.js': 'npm:decimal.js/decimal.min.js',
			
			'underscore': 'npm:underscore/underscore-min.js'
		},
		
		// packages tells the System loader how to load when no filename and/or no extension
		packages: {
			app: {
				main: './main.js',
				defaultExtension: 'js'
			},
			rxjs: {
				defaultExtension: 'js'
			},
			'angular2-contextmenu': {
				main: 'angular2-contextmenu.js',
				defaultExtension: 'js'
			},
			
			'ng2-ace-editor': {
				main: 'ng2-ace-editor.js',
				defaultExtension: 'js'
			},
			'brace': {
		        main: './index.js',
		        defaultExtension: 'js',
		        format: 'cjs'
			},
			'buffer': {
				main: './index.js',
		        defaultExtension: 'js',
		        format: 'cjs'
		    },
		    'w3c-blob': {
		    	main: './index.js',
		        defaultExtension: 'js',
		        format: 'cjs'
		    }
		}
	});
})(this);

