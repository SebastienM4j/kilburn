import { NgModule }               from '@angular/core';
import { CommonModule }           from '@angular/common';
import { FormsModule }            from '@angular/forms';

import { NgbModule }              from '@ng-bootstrap/ng-bootstrap';

import { ContextMenuModule }      from 'angular2-contextmenu';

import { UtilModule }             from '../../util/util.module';

import { CompteRoutingModule }    from './compte.routing-module';

import { CompteListComponent }    from './compte-list.component';
import { CompteFormComponent }    from './compte-form.component';

import { MessageDialogComponent } from '../../util/message-dialog.component';


@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      NgbModule,
      ContextMenuModule,
      UtilModule,
      CompteRoutingModule
  ],
  declarations: [
      CompteListComponent,
      CompteFormComponent
  ],
  entryComponents: [
      MessageDialogComponent
  ]
})
export class CompteModule {}
