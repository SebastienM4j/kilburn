import { Component }                  from '@angular/core';
import { Router }                     from '@angular/router';

import { NgbModal }                   from '@ng-bootstrap/ng-bootstrap';

import * as _                         from 'underscore';

import { CompteBackendClientService } from '../../service/compte-backend-client.service';

import { MessageDialogComponent }     from '../../util/message-dialog.component';
import { MessageDialogType }          from '../../util/message-dialog.component';
import { MessageDialogColor }         from '../../util/message-dialog.component';


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'compte-list',
    templateUrl: 'compte-list.component.html',
    providers: [ CompteBackendClientService ]
})
export class CompteListComponent
{
    comptes: Object[];
    displayedComptes: Object[];
    
    filters: boolean;
    textFilter: string;
    typeFilter: Object[];
    tagFilter: Object[];
    
    constructor(private router: Router,
                private modal: NgbModal,
                private compteBackendClientService: CompteBackendClientService) {}
    
    ngOnInit()
    {
        this.compteBackendClientService.findAllWithSolde().subscribe(cpts =>
        {
            this.comptes = cpts;
            
            this.populateDisplayedComptes();
            
            this.filters = false;
            this.populateTypeFilter([]);
            this.populateTagFilter([]);
        });
    }
    
    populateDisplayedComptes()
    {
        this.displayedComptes = _.map(this.comptes, (c) =>
        {
            let cpt = Object.assign({}, c);
            switch(cpt['type']) {
                case 'CHEQUE':
                    cpt['typeLibelle'] = 'Chèque';
                    break;
                case 'LIVRET_A':
                    cpt['typeLibelle'] = 'Livret A';
                    break;
                default:
                    cpt['typeLibelle'] = cpt['type'];
                    break;
            }
            if(cpt['tags']) {
                cpt['tags'].sort((l: string, r: string) => l < r ? -1 : (l > r ? 1 : 0));
            }
            return cpt;
        });
    }
    
    populateTypeFilter(selected: string[])
    {
        this.typeFilter = _.map(_.uniq(_.pluck(this.displayedComptes, 'typeLibelle')), (f) =>
        {
            return { 'label': f, 'filter': (selected && selected.indexOf(f) !== -1) };
        });
        this.typeFilter.sort((l, r) => this.sortTypeOrTagFilter(l, r));
    }
    
    populateTagFilter(selected: string[])
    {
        this.tagFilter = _.map(_.uniq(_.flatten(_.pluck(this.displayedComptes, 'tags'))), (f) =>
        {
            return { 'label': f, 'filter': (selected && selected.indexOf(f) !== -1) };
        });
        this.tagFilter.sort((l, r) => this.sortTypeOrTagFilter(l, r));
    }

    sortTypeOrTagFilter(l: Object, r: Object)
    {
        if(l['label'] < r['label']) {
            return -1;
        } else if(l['label'] > r['label']) {
            return 1;
        }
        return 0;
    }
    
    /*
     * Filtre
     */

    filterByText(textFilter: string)
    {
        this.textFilter = textFilter;
        this.filter();
    }

    filterByTypeOrTag(filter: Object)
    {
        filter['filter'] = !filter['filter'];
        this.filter();
    }

    filter()
    {
        this.populateDisplayedComptes();

        if(this.textFilter)
        {
            let search = this.textFilter.toLowerCase();
            
            this.displayedComptes = _.filter(this.displayedComptes, (c) =>
            {
                if(c['libelle'].toLowerCase().indexOf(search) !== -1 ||
                   c['numero'].toLowerCase().indexOf(search) !== -1 ||
                   c['typeLibelle'].toLowerCase().indexOf(search) !== -1) {
                    return true;
                }
                
                if(c['tags'] && c['tags'].length > 0 &&
                   _.some(c['tags'], (t: string) => t.toLowerCase().indexOf(search) !== -1)) {
                    return true;
                }
                
                return false;
            });
        }

        let thiz = this;

        // Filtre par type
        
        thiz.displayedComptes = _.filter(thiz.displayedComptes, (c) =>
        {
            for(let f of thiz.typeFilter) {
                if(f['filter'] && c['typeLibelle'] !== f['label']) {
                    return false;
                }
            }
            return true;
        });

        // Filtre par tag
        
        thiz.displayedComptes = _.filter(thiz.displayedComptes, (c) =>
        {
            for(let f of thiz.tagFilter) {
                if(f['filter'] && !thiz.hasTag(c, f['label'])) {
                    return false;
                }
            }
            return true;
        });


        // On n'affiche que les modes/tags correspondant aux données affichées
        
        let selectedTypes: string[] = Object.assign([], thiz.typeFilter).filter(f => f['filter']).map(f => f['label']);
        thiz.populateTypeFilter(selectedTypes);

        let selectedTags: string[] = Object.assign([], thiz.tagFilter).filter(f => f['filter']).map(f => f['label']);
        thiz.populateTagFilter(selectedTags);
    }

    hasTag(cpt: Object, tag: string)
    {
        if(!cpt['tags'] || cpt['tags'].length === 0) {
            return false;
        }
        return _.some(cpt['tags'], (t) => t === tag);
    }
    
    /*
     * Action menu contextuel
     */
    
    gotoCompteForm(cpt: Object)
    {
        this.router.navigate(['comptes', cpt['id']]);
    }
    
    gotoOperationList(cpt: Object)
    {
        this.router.navigate(['comptes', cpt['id'], 'operations']);
    }
    
    deleteCompte(cpt: Object)
    {
        let confirmDialog = this.modal.open(MessageDialogComponent);
        confirmDialog.componentInstance.type = MessageDialogType.CONFIRM;
        confirmDialog.componentInstance.color = MessageDialogColor.WARNING;
        confirmDialog.componentInstance.message = 'Confirmez-vous la suppression définitive du compte n° '+cpt['numero']+' et des éléments qui lui sont associés ?';
        
        confirmDialog.result.then(() =>
        {
            this.compteBackendClientService.deleteCompte(cpt['id']).subscribe(() =>
            {
                this.comptes = _.reject(this.comptes, (c) => c['id'] === cpt['id']);
                this.filter();
            });
        }, () => {});
    }
    
}
