import { Component }                  from '@angular/core';
import { ActivatedRoute }             from '@angular/router';

import { NgbModal }                   from '@ng-bootstrap/ng-bootstrap';

import { Observable }                 from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import * as _                         from 'underscore';

import { CompteBackendClientService } from '../../service/compte-backend-client.service';

import { MessageDialogComponent }     from '../../util/message-dialog.component';
import { MessageDialogType }          from '../../util/message-dialog.component';
import { MessageDialogColor }         from '../../util/message-dialog.component';


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'compte-form',
    templateUrl: 'compte-form.component.html',
    providers: [ CompteBackendClientService ]
})
export class CompteFormComponent
{
    title: string;

    compte: Object;
    loaded: boolean;

    addingTag: string;
    existingTags: string[];
    searchTags = (text$: Observable<string>) =>
        text$.debounceTime(200)
        .distinctUntilChanged()
        .map(term => term === ''
               ? []
               : this.existingTags.filter(v => new RegExp(term, 'gi').test(v)).slice(0, 10));
    
    constructor(private route: ActivatedRoute,
                private modal: NgbModal,
                private compteBackendClientService: CompteBackendClientService)
    {
        this.compte = {};
        this.compte['tags'] = [];
        this.loaded = false;
        
        let id = this.route.snapshot.params['id'];
        if(id === 'ajout') {
            this.title = 'Nouveau compte';
        } else {
            this.title = 'Modification du compte #'+id;
        }
    }
    
    ngOnInit()
    {
        this.compteBackendClientService.getExistingTags().subscribe(tags =>
        {
            this.existingTags = tags;
        });
        
        let id = this.route.snapshot.params['id'];
        if(id !== 'ajout')
        {
            this.compteBackendClientService.getCompte(id).subscribe(cpt =>
            {
                this.compte = cpt;
                this.loaded = true;
            });
        }
    }
    
    
    /*
     * Ajout / Suppression d'un tag
     */
    
    handleTagEnter(tag: string)
    {
        if(this.addingTag && this.addingTag.length > 0) {
            this.compte['tags'].push(this.addingTag);
            this.addingTag = '';
        }
    }
    
    handleTagClick()
    {
        if(this.addingTag && this.addingTag.length > 0) {
            this.compte['tags'].push(this.addingTag);
            this.addingTag = '';
        }
    }
    
    deleteTag(t: string)
    {
        this.compte['tags'] = _.without(this.compte['tags'], t);
    }
    
    /*
     * Enregistrement / Annulation
     */
    
    onSaveSuccess()
    {
        let dialog = this.modal.open(MessageDialogComponent);
        dialog.componentInstance.type = MessageDialogType.SUCCESS;
        dialog.componentInstance.message = 'Compte enregistré';
    }
    
    save()
    {
        let id = this.route.snapshot.params['id'];
        if(id === 'ajout') {
            this.compteBackendClientService.addCompte(this.compte).subscribe(() => this.onSaveSuccess());
        } else {
            this.compteBackendClientService.updateCompte(this.compte).subscribe(() => this.onSaveSuccess());
        }
    }
    
    cancel()
    {
        let id = this.route.snapshot.params['id'];
        if(id === 'ajout') {
            this.compte = {};
            this.compte['tags'] = [];
        } else {
            this.compteBackendClientService.getCompte(id).subscribe(cpt =>
            {
                this.compte = cpt;
            });
        }
    }
}
