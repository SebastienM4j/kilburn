import { NgModule }             from '@angular/core';
import { RouterModule }         from '@angular/router';
import { Routes }               from '@angular/router';

import { CompteListComponent }  from './compte-list.component';
import { CompteFormComponent }  from './compte-form.component';


const routes: Routes = [
    {
        path: 'comptes',
        component: CompteListComponent
    },
    {
        path: 'comptes/:id',
        component: CompteFormComponent
    }
];


@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class CompteRoutingModule {}
