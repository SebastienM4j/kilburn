import { NgModule }                           from '@angular/core';
import { RouterModule }                       from '@angular/router';
import { Routes }                             from '@angular/router';

import { RegleListComponent }                 from './regle-list.component';
import { RegleFormComponent }                 from './regle-form.component';


const routes: Routes = [
    {
        path: 'regles',
        component: RegleListComponent
    }
];


@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class RegleRoutingModule {}
