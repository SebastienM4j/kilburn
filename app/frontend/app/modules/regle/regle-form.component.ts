import { Component }                          from '@angular/core';
import { ViewChild }                          from '@angular/core';
import { Input }                              from '@angular/core';
import { Output }                             from '@angular/core';
import { EventEmitter }                       from '@angular/core';
import { SimpleChanges }                      from '@angular/core';

import { NgbModal }                           from '@ng-bootstrap/ng-bootstrap';

import "brace";
import "brace/mode/groovy";
import "brace/mode/javascript";
import "brace/theme/chrome";

import { Observable }                         from 'rxjs/Observable';

import { OperationRegleBackendClientService } from '../../service/operationregle-backend-client.service';

import { MessageDialogComponent }             from '../../util/message-dialog.component';
import { MessageDialogType }                  from '../../util/message-dialog.component';
import { MessageDialogColor }                 from '../../util/message-dialog.component';


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'regle-form',
    templateUrl: 'regle-form.component.html',
    providers: [ OperationRegleBackendClientService ]
})
export class RegleFormComponent
{
    @ViewChild('scriptEditor') scriptEditor: any;

    @Input() mode: string;
    @Input() regle: Object;

    @Output('onSave') onSaveEvent = new EventEmitter<Object>();
    @Output('onClose') onCloseEvent = new EventEmitter<void>();

    title: string;

    editorMode: string;
    editorOptions: Object;

    scriptValid: boolean;

    constructor(private modal: NgbModal,
                private operationRegleBackendClientService: OperationRegleBackendClientService)
    {
        this.scriptValid = true;

        this.editorOptions = {
            printMargin: false
        };
    }

    init()
    {
        this.scriptValid = true;

        if(this.mode === 'new')
        {
            this.title = 'Nouvelle règle';

            switch(this.regle['regle'])
            {
                case 'xyz.sebastienm4j.kilburn.api.operation.domain.regle.AmendOperationRegle':
                    this.regle['script'] = 'def amend(operation)\n'
                                         + '{\n'
                                         + '    \n'
                                         + '}';
                    break;

                case 'xyz.sebastienm4j.kilburn.api.operation.domain.regle.PackageOperationRegle':
                    this.regle['script'] = 'def packaging(List<Operation> operations)\n'
                        + '{\n'
                        + '    return operations\n'
                        + '}';
                    break;
            }
        }
        else
        {
            this.title = 'Modification de la règle #'+this.regle['id'];
        }

        this.langageChange();
        this.scriptEditor.getEditor().clearSelection();
    }

    ngOnInit()
    {
        this.init();
    }

    ngOnChanges(changes: SimpleChanges)
    {
        this.init();
    }

    /*
     * Formulaire
     */

    langageChange()
    {
        if(this.regle['langage'] == 'JAVASCRIPT') {
            this.editorMode = 'javascript';
        } else {
            this.editorMode = 'groovy';
        }
    }

    /*
     * Enregistrement / Annulation
     */

    onSaveSuccess(savedRegle: Object)
    {
        let dialog = this.modal.open(MessageDialogComponent);
        dialog.componentInstance.type = MessageDialogType.SUCCESS;
        dialog.componentInstance.message = 'Règle enregistrée';

        this.onSaveEvent.emit(savedRegle);
    }

    save()
    {
        this.scriptValid = (this.regle['script'] && this.regle['script'].length > 0);
        if(!this.scriptValid) {
            return;
        }

        this.operationRegleBackendClientService.checkScript(this.regle).subscribe(v =>
        {
            if(v['valid'])
            {
                if(this.mode === 'new') {
                    this.operationRegleBackendClientService.addOperationRegle(this.regle).subscribe(r => this.onSaveSuccess(r));
                } else {
                    this.operationRegleBackendClientService.updateOperationRegle(this.regle).subscribe(() => this.onSaveSuccess(this.regle));
                }
            }
            else
            {
                this.scriptValid = false;

                let dialog = this.modal.open(MessageDialogComponent, { size: 'lg' });
                dialog.componentInstance.type = MessageDialogType.ERROR;
                dialog.componentInstance.message = 'Le script n\'est pas valide : '+v['error'];
            }
        });
    }

    cancel()
    {
        if(this.mode === 'new') {
            let type = this.regle['regle'];
            this.regle = {
                regle: type,
                langage: 'GROOVY'
            };
            this.init();
        } else {
            this.operationRegleBackendClientService.getOperationRegle(this.regle['id']).subscribe(r =>
            {
                this.regle = r;
                this.init();
            });
        }
    }

    close()
    {
        this.onCloseEvent.emit();
    }

}
