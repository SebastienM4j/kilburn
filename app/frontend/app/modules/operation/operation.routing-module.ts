import { NgModule }                      from '@angular/core';
import { RouterModule }                  from '@angular/router';
import { Routes }                        from '@angular/router';

import { OperationListComponent }        from './operation-list.component';

import { SoldeResolver }                 from './operation.routing-resolvers';
import { OperationsResolver }            from './operation.routing-resolvers';

import { CompteBackendClientService }    from '../../service/compte-backend-client.service';
import { OperationBackendClientService } from '../../service/operation-backend-client.service';


const routes: Routes = [
    {
        path: 'comptes/:id/operations',
        component: OperationListComponent,
        resolve: {
            solde: SoldeResolver,
            operations: OperationsResolver
        }
    }
];


@NgModule({
  imports: [
      RouterModule.forChild(routes)
  ],
  providers: [
      SoldeResolver,
      OperationsResolver,
      CompteBackendClientService,
      OperationBackendClientService
  ],
  exports: [
      RouterModule
  ]
})
export class OperationRoutingModule {}
