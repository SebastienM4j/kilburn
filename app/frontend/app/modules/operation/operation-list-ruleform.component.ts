import { Component }                          from '@angular/core';
import { Output }                             from '@angular/core';
import { EventEmitter }                       from '@angular/core';

import { OperationRegleBackendClientService } from '../../service/operationregle-backend-client.service';


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'operation-list-ruleform',
    templateUrl: 'operation-list-ruleform.component.html',
    providers: [
        OperationRegleBackendClientService
    ]
})
export class OperationListRuleFormComponent
{
    @Output('onApplyRule') applyRuleEvent = new EventEmitter<Object>();
    @Output('onCancel') cancelEvent = new EventEmitter();

    packageRegles: Object[];
    selectedPackageRegle: Object;

    amendRegles: Object[];
    selectedAmendRegle: Object;

    constructor(private operationRegleBackendClientService: OperationRegleBackendClientService)
    {
        this.packageRegles = [];
        this.selectedPackageRegle = null;

        this.amendRegles = [];
        this.selectedAmendRegle = null;
    }

    ngOnInit()
    {
        this.operationRegleBackendClientService.findAllByTypeFull('xyz.sebastienm4j.kilburn.api.operation.domain.regle.PackageOperationRegle')
                                               .subscribe(regles => this.packageRegles = regles);

        this.operationRegleBackendClientService.findAllByTypeFull('xyz.sebastienm4j.kilburn.api.operation.domain.regle.AmendOperationRegle')
                                               .subscribe(regles => this.amendRegles = regles);
    }


    selectPackageRegle(r: Object)
    {
        this.selectedPackageRegle = r;
        this.selectedAmendRegle = null;
    }

    selectAmendRegle(r: Object)
    {
        this.selectedAmendRegle = r;
        console.log('this.selectedAmendRegle.description='+this.selectedAmendRegle['description']);
        this.selectedPackageRegle = null;
    }

    apply()
    {
        let regle: Object = null;
        if(this.selectedPackageRegle) {
            regle = this.selectedPackageRegle;
        } else if(this.selectedAmendRegle) {
            regle = this.selectedAmendRegle;
        }

        if(regle) {
            this.applyRuleEvent.emit(regle);
        }
    }

    cancel()
    {
        this.cancelEvent.emit();
    }

}
