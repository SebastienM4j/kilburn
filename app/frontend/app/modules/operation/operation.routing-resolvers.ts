import { Injectable }                         from '@angular/core';
import { Router }                             from '@angular/router';
import { Resolve }                            from '@angular/router';
import { RouterStateSnapshot }                from '@angular/router';
import { ActivatedRouteSnapshot }             from '@angular/router';

import { Observable }                         from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import { CompteBackendClientService }         from '../../service/compte-backend-client.service';
import { OperationBackendClientService }      from '../../service/operation-backend-client.service';


@Injectable()
export class SoldeResolver implements Resolve<Object>
{
    constructor(private router: Router,
                private compteBackendClientService: CompteBackendClientService) {}
    
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Object>
    {
        let id = route.params['id'];

        return this.compteBackendClientService.getCompteSolde(id).do(r => { return r });
    }
}

@Injectable()
export class OperationsResolver implements Resolve<Object>
{
    constructor(private router: Router,
                private operationBackendClientService: OperationBackendClientService) {}
    
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Object>
    {
        let id = route.params['id'];

        return this.operationBackendClientService.findAllOrderByDateDesc(id).do(r => { return r });
    }
}

