import { NgModule }               from '@angular/core';
import { LOCALE_ID }              from '@angular/core';
import { HttpModule }             from '@angular/http';
import { BrowserModule }          from '@angular/platform-browser';

import { NgbModule }              from '@ng-bootstrap/ng-bootstrap';

import { ClickOutsideDirective }  from 'angular2-click-outside';
import { ContextMenuModule }      from 'angular2-contextmenu';

import { UtilModule }             from './util/util.module';
import { MessageDialogComponent } from './util/message-dialog.component';

import { AppRoutingModule }       from './app.routing-module';
import { CompteModule }           from './modules/compte/compte.module';
import { OperationModule }	      from './modules/operation/operation.module';
import { RegleModule }            from './modules/regle/regle.module';

import { AppComponent }           from './app.component';
import { NavbarComponent }        from './navbar.component';
import { SidebarComponent }       from './sidebar.component';


@NgModule({
  imports: [
      HttpModule,
      BrowserModule, 
      NgbModule.forRoot(),
      ContextMenuModule.forRoot({ useBootstrap4: true }),
      AppRoutingModule,
      UtilModule,
      CompteModule,
      OperationModule,
      RegleModule
  ],
  declarations: [
      AppComponent,
      NavbarComponent,
      SidebarComponent,
      ClickOutsideDirective,
      MessageDialogComponent,
  ],
  providers: [
      { provide: LOCALE_ID, useValue: 'fr-FR' }
  ],
  bootstrap: [
      AppComponent
  ]
})
export class AppModule {}

