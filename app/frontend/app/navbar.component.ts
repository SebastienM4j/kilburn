import { Component }     from '@angular/core';
import { Input }         from '@angular/core';
import { Output }        from '@angular/core';
import { EventEmitter }  from '@angular/core';
import { SimpleChanges } from '@angular/core';


@Component({
    moduleId: module.id.replace('js/app', ''),
    selector: 'navbar',
    templateUrl: 'navbar.component.html',
})
export class NavbarComponent
{
    @Input() sidebarWidth: number;
    @Input('isSidebarAnchored') sidebarAnchored: boolean;
    
    @Output('onOpenSidebar') openSidebarEvent = new EventEmitter();
    
    left: number;
    
    ngOnInit()
    {
        this.sidebarAnchoring(this.sidebarAnchored);
    }
    
    ngOnChanges(changes: SimpleChanges)
    {
        for(let property in changes)
        {
            if(property === 'sidebarAnchored') {
                this.sidebarAnchoring(changes[property].currentValue);
            }
        }
    }
    
    sidebarAnchoring(anchored: boolean)
    {
        this.left = !anchored ? 0 : this.sidebarWidth;
    }
    
    openSidebar()
    {
        this.openSidebarEvent.emit();
    }
    
}
