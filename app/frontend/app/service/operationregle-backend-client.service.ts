import { Injectable }           from '@angular/core';
import { Http }                 from '@angular/http';
import { Response }             from '@angular/http';
import { URLSearchParams }      from '@angular/http';

import { Observable }           from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { BackendClientService } from './backend-client.service';


@Injectable()
export class OperationRegleBackendClientService extends BackendClientService
{
    private baseUrl = '/rest/backend/regles/operation';
    
    constructor (private http: Http)
    {
        super();
    }
    
    
    addOperationRegle(regle: Object): Observable<Object>
    {
        return this.http.post(this.baseUrl, regle)
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
    getOperationRegle(id: number): Observable<Object>
    {
        return this.http.get(this.baseUrl+'/'+id)
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
    updateOperationRegle(regle: Object): Observable<void>
    {
        return this.http.put(this.baseUrl+'/'+regle['id'], regle)
                        .catch(this.handleError);
    }
    
    deleteOperationRegle(id: number): Observable<void>
    {
        return this.http.delete(this.baseUrl+'/'+id)
                        .catch(this.handleError);
    }
    
    reorderOperationRegle(id: number, position: number): Observable<void>
    {
        return this.http.patch(this.baseUrl+'/'+id+'/ordre', { position: position })
                        .catch(this.handleError);
    }
    
    applyOperationRegle(id: number, operationIds: number[]): Observable<void>
    {
        return this.http.post(this.baseUrl+'/'+id+'/applications', operationIds)
                        .catch(this.handleError);
    }
    
    checkScript(regle: Object): Observable<Object>
    {
        return this.http.post(this.baseUrl+'/script-validations', regle)
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
    getOperationRegleTypes(): Observable<string[]>
    {
        return this.http.get(this.baseUrl+'/types')
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
    findAllByType(type: string): Observable<Object[]>
    {
        return this.http.get(this.baseUrl+'/types/'+type)
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
    findAllByTypeFull(type: string): Observable<Object[]>
    {
        let params: URLSearchParams = new URLSearchParams();
        params.set('full', 'true');
    
        return this.http.get(this.baseUrl+'/types/'+type, { search: params })
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
}
