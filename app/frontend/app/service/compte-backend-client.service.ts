import { Injectable }           from '@angular/core';
import { Http }                 from '@angular/http';
import { Response }             from '@angular/http';

import { Observable }           from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { BackendClientService } from './backend-client.service';


@Injectable()
export class CompteBackendClientService extends BackendClientService
{
    private baseUrl = '/rest/backend/comptes';
    
    constructor (private http: Http)
    {
        super();
    }
    
    
    findAllWithSolde(): Observable<Object[]>
    {
        return this.http.get(this.baseUrl)
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
    addCompte(compte: Object): Observable<void>
    {
        return this.http.post(this.baseUrl, compte)
                        .catch(this.handleError);
    }
    
    getCompte(id: number): Observable<Object[]>
    {
        return this.http.get(this.baseUrl+'/'+id)
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
    updateCompte(compte: Object): Observable<void>
    {
        return this.http.put(this.baseUrl+'/'+compte['id'], compte)
                        .catch(this.handleError);
    }
    
    deleteCompte(id: number): Observable<void>
    {
        return this.http.delete(this.baseUrl+'/'+id)
                        .catch(this.handleError);
    }
    
    getCompteSolde(id: number): Observable<Object>
    {
        return this.http.get(this.baseUrl+'/'+id+'/solde')
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
    getExistingTags(): Observable<string[]>
    {
        return this.http.get(this.baseUrl+'/tags')
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    
}
