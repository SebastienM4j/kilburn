package xyz.sebastienm4j.kilburn.api.compte.domain.factory;

import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;

/**
 * Permet d'instancier les types d'objets concernant les opérations.
 */
public interface CompteDomainFactory
{

    /**
     * @return Une nouvelle instance de {@link Compte}.
     */
    Compte newCompte();

}
