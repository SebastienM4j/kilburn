package xyz.sebastienm4j.kilburn.api.operation.domain.factory;

import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;

/**
 * Permet d'instancier les types d'objets concernant les opérations.
 */
public interface OperationDomainFactory
{

    /**
     * @return Une nouvelle instance de {@link Operation}.
     */
    Operation newOperation();

    /**
     * @return Une nouvelle instance de {@link OperationSource}.
     */
    OperationSource newOperationSource();

}
