package xyz.sebastienm4j.kilburn.api.operation.domain.regle;

import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Provenance;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Type;

import java.util.List;

/**
 * Règle applicable à une liste d'opérations
 * ({@link Provenance#IMPORT}) et permettant de les
 * ventiler en plusieurs opérations ({@link Type#VENTILATION}),
 * ou en grouper plusieurs en une seule ({@link Type#GROUPE}).
 * Sinon les opérations peuvent restées unitaires ({@link Type#UNITAIRE}).
 */
@FunctionalInterface
public interface PackageOperationRegle
{

    /**
     * "Conditionne" (si besoin) les opérations transmises.
     * Si la règle ne s'applique à aucune des opérations,
     * elle retourne simplement la liste tel quel.
     *
     * @param operations Les opérations originales (non <tt>null</tt>, non vide)
     * @return Les opérations conditionnées (non <tt>null</tt>, non vide)
     */
    List<Operation> packaging(List<Operation> operations);

}
