package xyz.sebastienm4j.kilburn.api.operation.domain.regle;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Registre des règles applicables aux opérations.
 */
public enum OperationRegleRegistry
{
    AMEND(AmendOperationRegle.class),
    PACKAGE(PackageOperationRegle.class);


    private Class<?> regleType;


    private OperationRegleRegistry(Class<?> regleType)
    {
        this.regleType = regleType;
    }


    public Class<?> getRegleType() {
        return regleType;
    }


    public static List<Class<?>> regleTypes()
    {
        return Arrays.asList(values()).stream()
                                      .map(r -> r.regleType)
                                      .collect(Collectors.toList());
    }

    public static boolean hasType(String className)
    {
        return Arrays.asList(values()).stream()
                                      .map(r -> r.regleType.getName())
                                      .anyMatch(cn -> cn.equals(className));
    }

    public static OperationRegleRegistry getByType(String className)
    {
        return Arrays.asList(values()).stream()
                                      .filter(r -> r.regleType.getName().equals(className))
                                      .findFirst()
                                      .orElse(null);
    }
}
