package xyz.sebastienm4j.kilburn.api.operation.domain.regle;

/**
 * Règle applicable à une opération permettant d'en modifier les valeurs.
 *
 * <p>Chaque implémentation de la règle se verra proposer toutes
 * les opérations à traiter.
 */
@FunctionalInterface
public interface AmendOperationRegle
{

    /**
     * Modifie (si besoin) l'opération transmise.
     *
     * <p>Si la règle ne s'applique pas à cette opération, elle ne fait rien.
     *
     * @param operation Opération modifiable (non <tt>null</tt>)
     */
    void amend(AmendOperation operation);

}
