package xyz.sebastienm4j.kilburn.api.operation.domain;

import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Set;

/**
 * Réprésente une opération bancaire.
 */
public interface Operation
{
    public static enum Mode
    {
        CARTE,
        RETRAIT,
        VERSEMENT,
        CHEQUE,
        REMISE,
        VIREMENT,
        PRELEVEMENT
    }

    public static enum Provenance
    {
        IMPORT,
        MANUELLE,
        ECHEANCIER
    }

    public static enum Type
    {
        UNITAIRE,
        VENTILATION,
        GROUPE
    }


    Long getId();

    void setId(Long id);


    Compte getCompte();

    void setCompte(Compte compte);

    Instant getDate();

    void setDate(Instant date);

    Instant getDateValeur();

    void setDateValeur(Instant dateValeur);

    Mode getMode();

    void setMode(Mode mode);

    String getTiers();

    void setTiers(String tiers);

    String getLibelle();

    void setLibelle(String libelle);

    String getReference();

    void setReference(String reference);

    BigDecimal getMontant();

    void setMontant(BigDecimal montant);

    String getCommentaire();

    void setCommentaire(String commentaire);

    Set<String> getTags();

    void setTags(Set<String> tags);

    boolean isPointee();

    void setPointee(boolean pointee);

    Provenance getProvenance();

    void setProvenance(Provenance provenance);

    Type getType();

    void setType(Type type);


    List<OperationSource> getSources();

    void setSources(List<OperationSource> sources);

}
