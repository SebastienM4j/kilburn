package xyz.sebastienm4j.kilburn.api.operation.datasource;

/**
 * Exception survenant lors de la récupération
 * d'opération à partir d'une source de données.
 */
public class OperationDataSourceException extends Exception
{
    private static final long serialVersionUID = -3344852398131109910L;


    public OperationDataSourceException()
    {
        super();
    }

    public OperationDataSourceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public OperationDataSourceException(String message)
    {
        super(message);
    }

    public OperationDataSourceException(Throwable cause)
    {
        super(cause);
    }

}
