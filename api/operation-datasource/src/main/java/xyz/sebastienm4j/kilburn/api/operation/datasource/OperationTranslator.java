package xyz.sebastienm4j.kilburn.api.operation.datasource;

import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;

import java.util.Set;

/**
 * Permet de traduire une {@link OperationSource} en une {@link Operation}.
 */
public interface OperationTranslator
{

    /**
     * Indique si le traducteur est applicable à
     * l'opération source transmise.
     *
     * @param operation Opération à traduire (non <tt>null</tt>)
     * @return <tt>true</tt> si la traduction peut être réalisée
     */
    boolean isApplicableTo(OperationSource operation);

    /**
     * Traduit l'opération source.
     *
     * @param operation Opération à traduire (non <tt>null</tt>)
     * @param comtpes Liste des comptes connus dans laquelle rechercher celui correspondant à l'opération (non <tt>null</tt>).
     *                Si le compte n'est pas trouvé, l'opération n'est pas importée, une exception <tt>OperationTranslationException</tt>
     *                est levée.
     * @return L'opération traduite (non <tt>null</tt>
     * @throws OperationTranslationException En cas d'erreur lors de la traduction
     */
    Operation translate(OperationSource operation, Set<Compte> comptes) throws OperationTranslationException;

}
