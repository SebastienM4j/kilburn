package xyz.sebastienm4j.kilburn.api.operation.datasource;

/**
 * Exception survenant lors de la traduction d'une
 * opération source en opération.
 */
public class OperationTranslationException extends Exception
{
    private static final long serialVersionUID = -3518875256297079924L;


    public OperationTranslationException()
    {
        super();
    }

    public OperationTranslationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public OperationTranslationException(String message)
    {
        super(message);
    }

    public OperationTranslationException(Throwable cause)
    {
        super(cause);
    }

}
