package xyz.sebastienm4j.kilburn.api.operation.datasource;

import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;

import java.util.Collection;
import java.util.Map;

/**
 * Source de données permettant d'obtenir des opérations bancaires.
 */
public interface OperationDataSource
{

    /**
     * Récupère les "dernières" opérations bancaires
     * à partir de cette source de données.
     *
     * <p>Le sens des "dernières" opérations est laissé
     * à l'interprétation de l'implémentation en fonction
     * de ce quelle peut récupérer. Il peut s'agir de la
     * totalité des opérations, ou bien que d'un échantillon.
     *
     * <p>Les opérations récupérées peuvent déjà l'avoir été
     * auparavant. L'implémentation n'a pas necessairement
     * besoin de stocker la date de la dernière récupération.<br>
     * Ceci étant, elle est libre de le faire pour éventuellement
     * éviter de retourner systématiquement tout une liste
     * d'opérations déjà récupérées.
     *
     * <p>Les opérations récupérées peuvent concerner différents
     * comptes bancaires.
     *
     * @param dataSourceProperties Propriétés (configuration) de la source de données
     * @return Les opérations récupérées
     * @throws OperationDataSourceException En cas d'échec lors de la récupération
     */
    Collection<OperationSource> getLastOperations(Map<String, String> dataSourceProperties) throws OperationDataSourceException;

}
