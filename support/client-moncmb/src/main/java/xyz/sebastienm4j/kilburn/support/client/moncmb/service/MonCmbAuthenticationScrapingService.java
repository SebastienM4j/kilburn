package xyz.sebastienm4j.kilburn.support.client.moncmb.service;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.MonCmbAuthToken;
import xyz.sebastienm4j.kilburn.support.client.moncmb.exception.MonCmbAuthenticationException;
import xyz.sebastienm4j.kilburn.support.client.moncmb.scraping.MonCmbAuthTokenExtractor;
import xyz.sebastienm4j.kilburn.support.client.moncmb.scraping.WebConsoleLoggerBridge;

/**
 * Service d'authentification sur le site mon.cmb.fr par web scraping.
 *
 * @deprecated Supprimé au profit de {@link MonCmbAuthenticationApiService}
 * TODO à supprimer si MonCmbAuthenticationApiService s'avère plus pérenne
 */
@Service
@Deprecated
public class MonCmbAuthenticationScrapingService implements MonCmbAuthenticationService
{
    private static final Logger logger = LoggerFactory.getLogger(MonCmbAuthenticationScrapingService.class);


    @Override
    public MonCmbAuthToken authenticate(String login, String password) throws MonCmbAuthenticationException
    {
        Assert.hasText(login, "Identifiant absent ou vide");
        Assert.hasText(password, "Mot de passe absent ou vide");

        try(WebClient webClient = new WebClient())
        {
            logger.debug("Authentification [scraping] sur mon.cmb.fr avec le compte [{}/******]", login);

            webClient.getOptions().setCssEnabled(false);

            MonCmbAuthTokenExtractor authTokenExtractor = new MonCmbAuthTokenExtractor();
            WebConsoleLoggerBridge webConsoleLogger = new WebConsoleLoggerBridge();
            webConsoleLogger.setInfoEnabled(true);
            webConsoleLogger.addInfoListener(authTokenExtractor);
            webClient.getWebConsole().setLogger(webConsoleLogger);

            HtmlPage authPage = webClient.getPage("https://mon.cmb.fr/auth/login");

            // Mire

            HtmlButton buttonStart = (HtmlButton)authPage.getFirstByXPath("//header/div/div/login-area/div/div/button");
            buttonStart.click();

            // Login

            HtmlTextInput loginInput = (HtmlTextInput)authPage.getElementById("userLogin");
            loginInput.setText(login);

            HtmlForm formLogin = (HtmlForm)authPage.getElementById("auth-c_1");
            HtmlButton buttonNext = (HtmlButton)formLogin.getFirstByXPath("//div[@class='wrap-btn wrap-btn-1x']/button");
            buttonNext.click();

            // Password

            HtmlPasswordInput passwordInput = (HtmlPasswordInput)authPage.getElementById("userPassword");
            passwordInput.setText(password);

            HtmlForm formPassword = (HtmlForm)authPage.getElementById("formLogin");
            HtmlButton buttonCx = (HtmlButton)formPassword.getFirstByXPath("//div/div[@class='wrap-btn wrap-btn-1x']/button");
            buttonCx.click();


            String accessToken = authTokenExtractor.getAccessToken();
            String idToken = authTokenExtractor.getIdToken();

            if(StringUtils.hasText(accessToken) && StringUtils.hasText(idToken))
            {
                logger.debug("Authentification [scraping] sur mon.cmb.fr réussie, access_token [{}], id_token [{}]", accessToken, idToken);
                return new MonCmbAuthToken(accessToken, idToken);
            }

            throw new MonCmbAuthenticationException("Echec de l'authentification [scraping] sur mon.cmb.fr, tokens non obtenus");

        } catch(Exception ex) {
            throw new MonCmbAuthenticationException("Echec de l'authentification [scraping] sur mon.cmb.fr, erreur inconnue", ex);
        }
    }

}
