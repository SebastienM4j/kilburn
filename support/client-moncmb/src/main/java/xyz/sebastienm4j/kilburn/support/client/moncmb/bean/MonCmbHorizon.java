package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MonCmbHorizon
{
    private String libelle;
    @JsonProperty("savingsProducts")
    private List<MonCmbSavingsProduct> savingsProducts;
    private Double soldeHorizon;
    private String type;


    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public List<MonCmbSavingsProduct> getSavingsProducts() {
        return savingsProducts;
    }

    public void setSavingsProducts(List<MonCmbSavingsProduct> savingsProducts) {
        this.savingsProducts = savingsProducts;
    }

    public Double getSoldeHorizon() {
        return soldeHorizon;
    }

    public void setSoldeHorizon(Double soldeHorizon) {
        this.soldeHorizon = soldeHorizon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
