package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

public class MonCmbOperationEpargneProxy extends MonCmbBaseOperation
{
    private String operationId;
    private Object suggestedCategories;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public Object getSuggestedCategories() {
        return suggestedCategories;
    }

    public void setSuggestedCategories(Object suggestedCategories) {
        this.suggestedCategories = suggestedCategories;
    }
}
