package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

public class MonCmbInfosPerson
{
    private boolean capable;
    private String categoriePersonne;
    private String email;
    private String firstName;
    private String lastName;
    private String login;
    private String title;
    private int yearOfBirth;


    public boolean isCapable()
    {
        return capable;
    }

    public void setCapable(boolean capable)
    {
        this.capable = capable;
    }

    public String getCategoriePersonne()
    {
        return categoriePersonne;
    }

    public void setCategoriePersonne(String categoriePersonne)
    {
        this.categoriePersonne = categoriePersonne;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public int getYearOfBirth()
    {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth)
    {
        this.yearOfBirth = yearOfBirth;
    }

}
