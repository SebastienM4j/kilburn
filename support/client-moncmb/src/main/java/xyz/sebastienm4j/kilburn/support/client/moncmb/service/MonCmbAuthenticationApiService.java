package xyz.sebastienm4j.kilburn.support.client.moncmb.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import xyz.sebastienm4j.kilburn.shared.rest.client.RestClientService;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.MonCmbAuthToken;
import xyz.sebastienm4j.kilburn.support.client.moncmb.exception.MonCmbAuthenticationException;

import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.http.HttpStatus.FOUND;

/**
 * Service d'authentification sur le site mon.cmb.fr en utilisant l'api security.
 */
@Service
public class MonCmbAuthenticationApiService extends RestClientService implements MonCmbAuthenticationService
{
    private static final Logger logger = LoggerFactory.getLogger(MonCmbAuthenticationApiService.class);


    @Override
    public String baseUrl()
    {
        return "https://mon.cmb.fr/securityapi/tokens";
    }


    @Override
    public MonCmbAuthToken authenticate(String login, String password) throws MonCmbAuthenticationException
    {
        Assert.hasText(login, "Identifiant absent ou vide");
        Assert.hasText(password, "Mot de passe absent ou vide");

        try
        {
            logger.debug("Authentification [api] sur mon.cmb.fr avec le compte [{}/******]", login);

            RestTemplate template = restTemplate(FOUND);

            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
            headers.add(HttpHeaders.HOST, "mon.cmb.fr");
            headers.add(HttpHeaders.ORIGIN, "https://mon.cmb.fr");

            String body = "accessCode=" + login + "&" +
                    "password=" + password + "&" +
                    "clientId=com.arkea.cmb.siteaccessible&" +
                    "redirectUri=https%3A%2F%2Fmon.cmb.fr%2Fauth%2Fcheckuser&" +
                    "prospectRedirectUri=https%3A%2F%2Fouvrirmoncompte.cmb.fr%2Feer%2F&" +
                    "errorUri=https%3A%2F%2Fmon.cmb.fr%2Fauth%2Ferrorauthn&" +
                    "fingerPrint=8037da39a4d400f60a51d6d70a772e90";

            HttpEntity<String> request = new HttpEntity<>(body, headers);

            URI response = template.postForLocation(url(), request);

            String location = response.toString();
            if (!StringUtils.hasText(location)) {
                throw new MonCmbAuthenticationException("Echec de l'authentification [api] sur mon.cmb.fr, pas de location");
            }

            logger.debug("Authentification [api] sur mon.cmb.fr réussie, localtion [{}]", location);

            Pattern accessTokenPattern = Pattern.compile("^.*access_token=(.*)&.*$");
            Matcher accessTokenMatcher = accessTokenPattern.matcher(location);
            String accessToken = accessTokenMatcher.matches() ? accessTokenMatcher.group(1) : null;

            Pattern idTokenPattern = Pattern.compile("^.*id_token=(.*)$");
            Matcher idTokenMatcher = idTokenPattern.matcher(location);
            String idToken = idTokenMatcher.matches() ? idTokenMatcher.group(1) : null;

            if(StringUtils.hasText(accessToken) && StringUtils.hasText(idToken))
            {
                logger.debug("Authentification [api] sur mon.cmb.fr réussie, access_token [{}], id_token [{}]", accessToken, idToken);
                return new MonCmbAuthToken(accessToken, idToken);
            }

            throw new MonCmbAuthenticationException("Echec de l'authentification [api] sur mon.cmb.fr, tokens non obtenus");

        } catch(Exception ex) {
            throw new MonCmbAuthenticationException("Echec de l'authentification [api] sur mon.cmb.fr, erreur inconnue", ex);
        }
    }


}
