package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.LocalTime;

public class MonCmbCompteTitulaireCotitulaire
{
    public static class AccountTypeProxy
    {
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }

    private String identifiantTechniqueCrypte;
    private String lib;
    private String accountListType;
    private Double usedAmount;
    private String codeQualiteSignature;
    private boolean indicateurRibable;
    private Double soldeEuro;
    private String nomCotitulaire;
    @JsonProperty("AVenir")
    private Double aVenir;
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dateArreteSolde;
    private String codeTypePersonneTitulaire;
    private String numeroContratSouscrit;
    @JsonFormat(pattern="HH'h'mm")
    private LocalTime heureArreteSolde;
    private boolean eligibiliteCredit;
    private String mnemonic;
    private boolean eligibiliteDebit;
    private String prenomCotitulaire;
    private String deviseCompteCode;
    private AccountTypeProxy accountTypeProxy;
    private String prenomClient;
    private String accountType;
    private String index;
    private String identifiantTechnique;
    private Double autorisationDecouvert;
    private String iban;
    private String bic;
    private String nomClient;


    public String getIdentifiantTechniqueCrypte()
    {
        return identifiantTechniqueCrypte;
    }

    public void setIdentifiantTechniqueCrypte(String identifiantTechniqueCrypte)
    {
        this.identifiantTechniqueCrypte = identifiantTechniqueCrypte;
    }

    public String getLib()
    {
        return lib;
    }

    public void setLib(String lib)
    {
        this.lib = lib;
    }

    public String getAccountListType()
    {
        return accountListType;
    }

    public void setAccountListType(String accountListType)
    {
        this.accountListType = accountListType;
    }

    public Double getUsedAmount()
    {
        return usedAmount;
    }

    public void setUsedAmount(Double usedAmount)
    {
        this.usedAmount = usedAmount;
    }

    public String getCodeQualiteSignature()
    {
        return codeQualiteSignature;
    }

    public void setCodeQualiteSignature(String codeQualiteSignature)
    {
        this.codeQualiteSignature = codeQualiteSignature;
    }

    public boolean isIndicateurRibable()
    {
        return indicateurRibable;
    }

    public void setIndicateurRibable(boolean indicateurRibable)
    {
        this.indicateurRibable = indicateurRibable;
    }

    public Double getSoldeEuro()
    {
        return soldeEuro;
    }

    public void setSoldeEuro(Double soldeEuro)
    {
        this.soldeEuro = soldeEuro;
    }

    public String getNomCotitulaire()
    {
        return nomCotitulaire;
    }

    public void setNomCotitulaire(String nomCotitulaire)
    {
        this.nomCotitulaire = nomCotitulaire;
    }

    public Double getaVenir()
    {
        return aVenir;
    }

    public void setaVenir(Double aVenir)
    {
        this.aVenir = aVenir;
    }

    public LocalDate getDateArreteSolde()
    {
        return dateArreteSolde;
    }

    public void setDateArreteSolde(LocalDate dateArreteSolde)
    {
        this.dateArreteSolde = dateArreteSolde;
    }

    public String getCodeTypePersonneTitulaire()
    {
        return codeTypePersonneTitulaire;
    }

    public void setCodeTypePersonneTitulaire(String codeTypePersonneTitulaire)
    {
        this.codeTypePersonneTitulaire = codeTypePersonneTitulaire;
    }

    public String getNumeroContratSouscrit()
    {
        return numeroContratSouscrit;
    }

    public void setNumeroContratSouscrit(String numeroContratSouscrit)
    {
        this.numeroContratSouscrit = numeroContratSouscrit;
    }

    public LocalTime getHeureArreteSolde()
    {
        return heureArreteSolde;
    }

    public void setHeureArreteSolde(LocalTime heureArreteSolde)
    {
        this.heureArreteSolde = heureArreteSolde;
    }

    public boolean isEligibiliteCredit()
    {
        return eligibiliteCredit;
    }

    public void setEligibiliteCredit(boolean eligibiliteCredit)
    {
        this.eligibiliteCredit = eligibiliteCredit;
    }

    public String getMnemonic()
    {
        return mnemonic;
    }

    public void setMnemonic(String mnemonic)
    {
        this.mnemonic = mnemonic;
    }

    public boolean isEligibiliteDebit()
    {
        return eligibiliteDebit;
    }

    public void setEligibiliteDebit(boolean eligibiliteDebit)
    {
        this.eligibiliteDebit = eligibiliteDebit;
    }

    public String getPrenomCotitulaire()
    {
        return prenomCotitulaire;
    }

    public void setPrenomCotitulaire(String prenomCotitulaire)
    {
        this.prenomCotitulaire = prenomCotitulaire;
    }

    public String getDeviseCompteCode()
    {
        return deviseCompteCode;
    }

    public void setDeviseCompteCode(String deviseCompteCode)
    {
        this.deviseCompteCode = deviseCompteCode;
    }

    public AccountTypeProxy getAccountTypeProxy()
    {
        return accountTypeProxy;
    }

    public void setAccountTypeProxy(AccountTypeProxy accountTypeProxy)
    {
        this.accountTypeProxy = accountTypeProxy;
    }

    public String getPrenomClient()
    {
        return prenomClient;
    }

    public void setPrenomClient(String prenomClient)
    {
        this.prenomClient = prenomClient;
    }

    public String getAccountType()
    {
        return accountType;
    }

    public void setAccountType(String accountType)
    {
        this.accountType = accountType;
    }

    public String getIndex()
    {
        return index;
    }

    public void setIndex(String index)
    {
        this.index = index;
    }

    public String getIdentifiantTechnique()
    {
        return identifiantTechnique;
    }

    public void setIdentifiantTechnique(String identifiantTechnique)
    {
        this.identifiantTechnique = identifiantTechnique;
    }

    public Double getAutorisationDecouvert()
    {
        return autorisationDecouvert;
    }

    public void setAutorisationDecouvert(Double autorisationDecouvert)
    {
        this.autorisationDecouvert = autorisationDecouvert;
    }

    public String getIban()
    {
        return iban;
    }

    public void setIban(String iban)
    {
        this.iban = iban;
    }

    public String getBic()
    {
        return bic;
    }

    public void setBic(String bic)
    {
        this.bic = bic;
    }

    public String getNomClient()
    {
        return nomClient;
    }

    public void setNomClient(String nomClient)
    {
        this.nomClient = nomClient;
    }

}
