package xyz.sebastienm4j.kilburn.support.client.moncmb.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import xyz.sebastienm4j.kilburn.shared.json.config.JacksonSpringConfig;

/**
 * Configuration Spring pour le projet Client MonCmb.
 */
@Configuration
@Import(JacksonSpringConfig.class)
@ComponentScan("xyz.sebastienm4j.kilburn.support.client.moncmb.service")
public class SupportClientMonCmbSpringConfig
{

}
