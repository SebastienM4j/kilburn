package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MonCmbSyntheseEpargne
{
    private Double balance;
    private Boolean capable;
    @JsonProperty("horizons")
    private List<MonCmbHorizon> horizons;


    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Boolean getCapable() {
        return capable;
    }

    public void setCapable(Boolean capable) {
        this.capable = capable;
    }

    public List<MonCmbHorizon> getHorizons() {
        return horizons;
    }

    public void setHorizons(List<MonCmbHorizon> horizons) {
        this.horizons = horizons;
    }
}
