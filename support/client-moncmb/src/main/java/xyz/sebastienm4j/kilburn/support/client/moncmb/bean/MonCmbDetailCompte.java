package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import xyz.sebastienm4j.kilburn.shared.json.serialization.LocalDateTimeTimestampDeserializer;

import java.time.LocalDateTime;
import java.util.List;

public class MonCmbDetailCompte
{
    private Double autorisationDecouvert;
    private Double avenir;
    @JsonDeserialize(using=LocalDateTimeTimestampDeserializer.class)
    private LocalDateTime dateSolde;
    private String deviseCompte;
    private String libellePersonnalise;
    private String productInformationSheet;
    private List<MonCmbOperationProxy> listOperationProxy;


    public Double getAutorisationDecouvert()
    {
        return autorisationDecouvert;
    }

    public void setAutorisationDecouvert(Double autorisationDecouvert)
    {
        this.autorisationDecouvert = autorisationDecouvert;
    }

    public Double getAvenir()
    {
        return avenir;
    }

    public void setAvenir(Double avenir)
    {
        this.avenir = avenir;
    }

    public LocalDateTime getDateSolde()
    {
        return dateSolde;
    }

    public void setDateSolde(LocalDateTime dateSolde)
    {
        this.dateSolde = dateSolde;
    }

    public String getDeviseCompte()
    {
        return deviseCompte;
    }

    public void setDeviseCompte(String deviseCompte)
    {
        this.deviseCompte = deviseCompte;
    }

    public String getLibellePersonnalise()
    {
        return libellePersonnalise;
    }

    public void setLibellePersonnalise(String libellePersonnalise)
    {
        this.libellePersonnalise = libellePersonnalise;
    }

    public String getProductInformationSheet()
    {
        return productInformationSheet;
    }

    public void setProductInformationSheet(String productInformationSheet)
    {
        this.productInformationSheet = productInformationSheet;
    }

    public List<MonCmbOperationProxy> getListOperationProxy()
    {
        return listOperationProxy;
    }

    public void setListOperationProxy(List<MonCmbOperationProxy> listOperationProxy)
    {
        this.listOperationProxy = listOperationProxy;
    }

}
