package xyz.sebastienm4j.kilburn.support.client.moncmb.scraping;

import xyz.sebastienm4j.kilburn.support.client.moncmb.service.MonCmbAuthenticationScrapingService;

import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Listener pour pour extraire les informations d'authentification
 * à mon.cmb.fr via {@link WebConsoleLoggerBridge} lors du scraping
 * du site (cf {@link MonCmbAuthenticationScrapingService#authenticate(String, String)}.
 */
public class MonCmbAuthTokenExtractor implements Consumer<String>
{
    private String accessToken;
    private String idToken;


    @Override
    public void accept(String message)
    {
        Pattern pattern = Pattern.compile("\\(\\{access_token:\\\"(.*)\\\", id_token:\\\"(.*)\\\"\\}\\)");
        Matcher matcher = pattern.matcher(message);

        if(matcher.matches())
        {
            accessToken = matcher.group(1);
            idToken = matcher.group(2);
        }
    }


    public String getAccessToken()
    {
        return accessToken;
    }

    public String getIdToken()
    {
        return idToken;
    }
}
