package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

public class MonCmbDetailComptePayload
{
    public static enum FiltreOperations
    {
        CINQ_DERNIERES_OPERATIONS,
        SIX_DERNIERES_SEMAINES,
        MOIS_MOINS_UN,
        MOIS_MOINS_DEUX,
        MOIS_MOINS_TROIS,
        MOIS_MOINS_QUATRE,
        MOIS_MOINS_CINQ,
        MOIS_MOINS_SIX,
        MOIS_MOINS_SEPT,
        MOIS_MOINS_HUIT,
        MOIS_MOINS_NEUF,
        MOIS_MOINS_DIX,
        MOIS_MOINS_ONZE,
        MOIS_MOINS_DOUZE
    }

    private FiltreOperations filtreOperationsComptabilisees;
    private String index;
    private boolean isNonBloquant;
    private boolean callEtalis;
    private boolean compteCheque;
    private boolean prefAccount;


    public FiltreOperations getFiltreOperationsComptabilisees()
    {
        return filtreOperationsComptabilisees;
    }

    public void setFiltreOperationsComptabilisees(FiltreOperations filtreOperationsComptabilisees)
    {
        this.filtreOperationsComptabilisees = filtreOperationsComptabilisees;
    }

    public String getIndex()
    {
        return index;
    }

    public void setIndex(String index)
    {
        this.index = index;
    }

    public boolean isNonBloquant()
    {
        return isNonBloquant;
    }

    public void setNonBloquant(boolean isNonBloquant)
    {
        this.isNonBloquant = isNonBloquant;
    }

    public boolean isCallEtalis()
    {
        return callEtalis;
    }

    public void setCallEtalis(boolean callEtalis)
    {
        this.callEtalis = callEtalis;
    }

    public boolean isCompteCheque()
    {
        return compteCheque;
    }

    public void setCompteCheque(boolean compteCheque)
    {
        this.compteCheque = compteCheque;
    }

    public boolean isPrefAccount()
    {
        return prefAccount;
    }

    public void setPrefAccount(boolean prefAccount)
    {
        this.prefAccount = prefAccount;
    }

}
