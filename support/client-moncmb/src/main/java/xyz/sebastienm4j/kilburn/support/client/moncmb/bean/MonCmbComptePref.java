package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class MonCmbComptePref
{
    private Double grantedAmount;
    private String identifiantTechniqueCrypte;
    @JsonProperty("mapCodeRequeteCodeEtatPreference")
    private Map<String, String> mapCodeRequeteCodeEtatPreference;
    private String lib;
    private String accountListType;
    private Double usedAmount;
    private String codeQualiteSignature;
    private boolean indicateurRibable;
    private Double soldeEuro;;
    private String nomCotitulaire;
    private String codeTypePersonneTitulaire;
    private String numeroContratSouscrit;
    private String mnemonic;
    private String solde;
    private boolean eligibiliteDebit;
    private String prenomCotitulaire;
    private String deviseCompteCode;
    private Double ceiling;
    private String prenomClient;
    private String accountType;
    private Integer curentMonthly;
    private String index;
    private String levyAccount;
    private String identifiantTechnique;
    private String iban;
    private String releaseState;
    private String bic;
    private String nomClient;


    public Double getGrantedAmount()
    {
        return grantedAmount;
    }

    public void setGrantedAmount(Double grantedAmount)
    {
        this.grantedAmount = grantedAmount;
    }

    public String getIdentifiantTechniqueCrypte()
    {
        return identifiantTechniqueCrypte;
    }

    public void setIdentifiantTechniqueCrypte(String identifiantTechniqueCrypte)
    {
        this.identifiantTechniqueCrypte = identifiantTechniqueCrypte;
    }

    public Map<String, String> getMapCodeRequeteCodeEtatPreference()
    {
        return mapCodeRequeteCodeEtatPreference;
    }

    public void setMapCodeRequeteCodeEtatPreference(
            Map<String, String> mapCodeRequeteCodeEtatPreference)
    {
        this.mapCodeRequeteCodeEtatPreference = mapCodeRequeteCodeEtatPreference;
    }

    public String getLib()
    {
        return lib;
    }

    public void setLib(String lib)
    {
        this.lib = lib;
    }

    public String getAccountListType()
    {
        return accountListType;
    }

    public void setAccountListType(String accountListType)
    {
        this.accountListType = accountListType;
    }

    public Double getUsedAmount()
    {
        return usedAmount;
    }

    public void setUsedAmount(Double usedAmount)
    {
        this.usedAmount = usedAmount;
    }

    public String getCodeQualiteSignature()
    {
        return codeQualiteSignature;
    }

    public void setCodeQualiteSignature(String codeQualiteSignature)
    {
        this.codeQualiteSignature = codeQualiteSignature;
    }

    public boolean isIndicateurRibable()
    {
        return indicateurRibable;
    }

    public void setIndicateurRibable(boolean indicateurRibable)
    {
        this.indicateurRibable = indicateurRibable;
    }

    public Double getSoldeEuro()
    {
        return soldeEuro;
    }

    public void setSoldeEuro(Double soldeEuro)
    {
        this.soldeEuro = soldeEuro;
    }

    public String getNomCotitulaire()
    {
        return nomCotitulaire;
    }

    public void setNomCotitulaire(String nomCotitulaire)
    {
        this.nomCotitulaire = nomCotitulaire;
    }

    public String getCodeTypePersonneTitulaire()
    {
        return codeTypePersonneTitulaire;
    }

    public void setCodeTypePersonneTitulaire(String codeTypePersonneTitulaire)
    {
        this.codeTypePersonneTitulaire = codeTypePersonneTitulaire;
    }

    public String getNumeroContratSouscrit()
    {
        return numeroContratSouscrit;
    }

    public void setNumeroContratSouscrit(String numeroContratSouscrit)
    {
        this.numeroContratSouscrit = numeroContratSouscrit;
    }

    public String getMnemonic()
    {
        return mnemonic;
    }

    public void setMnemonic(String mnemonic)
    {
        this.mnemonic = mnemonic;
    }

    public String getSolde()
    {
        return solde;
    }

    public void setSolde(String solde)
    {
        this.solde = solde;
    }

    public boolean isEligibiliteDebit()
    {
        return eligibiliteDebit;
    }

    public void setEligibiliteDebit(boolean eligibiliteDebit)
    {
        this.eligibiliteDebit = eligibiliteDebit;
    }

    public String getPrenomCotitulaire()
    {
        return prenomCotitulaire;
    }

    public void setPrenomCotitulaire(String prenomCotitulaire)
    {
        this.prenomCotitulaire = prenomCotitulaire;
    }

    public String getDeviseCompteCode()
    {
        return deviseCompteCode;
    }

    public void setDeviseCompteCode(String deviseCompteCode)
    {
        this.deviseCompteCode = deviseCompteCode;
    }

    public Double getCeiling()
    {
        return ceiling;
    }

    public void setCeiling(Double ceiling)
    {
        this.ceiling = ceiling;
    }

    public String getPrenomClient()
    {
        return prenomClient;
    }

    public void setPrenomClient(String prenomClient)
    {
        this.prenomClient = prenomClient;
    }

    public String getAccountType()
    {
        return accountType;
    }

    public void setAccountType(String accountType)
    {
        this.accountType = accountType;
    }

    public Integer getCurentMonthly()
    {
        return curentMonthly;
    }

    public void setCurentMonthly(Integer curentMonthly)
    {
        this.curentMonthly = curentMonthly;
    }

    public String getIndex()
    {
        return index;
    }

    public void setIndex(String index)
    {
        this.index = index;
    }

    public String getLevyAccount()
    {
        return levyAccount;
    }

    public void setLevyAccount(String levyAccount)
    {
        this.levyAccount = levyAccount;
    }

    public String getIdentifiantTechnique()
    {
        return identifiantTechnique;
    }

    public void setIdentifiantTechnique(String identifiantTechnique)
    {
        this.identifiantTechnique = identifiantTechnique;
    }

    public String getIban()
    {
        return iban;
    }

    public void setIban(String iban)
    {
        this.iban = iban;
    }

    public String getReleaseState()
    {
        return releaseState;
    }

    public void setReleaseState(String releaseState)
    {
        this.releaseState = releaseState;
    }

    public String getBic()
    {
        return bic;
    }

    public void setBic(String bic)
    {
        this.bic = bic;
    }

    public String getNomClient()
    {
        return nomClient;
    }

    public void setNomClient(String nomClient)
    {
        this.nomClient = nomClient;
    }

}
