package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MonCmbDetailCompteEpargne
{
    private Double ceiling;
    private Double floor;
    private String libellePersonnalise;
    @JsonProperty("listOperationProxy")
    private List<MonCmbOperationEpargneProxy> listOperationProxy;
    private Boolean pointageCategoriesOpe;
    private String productInformationSheet;
    private Double rate;


    public Double getCeiling() {
        return ceiling;
    }

    public void setCeiling(Double ceiling) {
        this.ceiling = ceiling;
    }

    public Double getFloor() {
        return floor;
    }

    public void setFloor(Double floor) {
        this.floor = floor;
    }

    public String getLibellePersonnalise() {
        return libellePersonnalise;
    }

    public void setLibellePersonnalise(String libellePersonnalise) {
        this.libellePersonnalise = libellePersonnalise;
    }

    public List<MonCmbOperationEpargneProxy> getListOperationProxy() {
        return listOperationProxy;
    }

    public void setListOperationProxy(List<MonCmbOperationEpargneProxy> listOperationProxy) {
        this.listOperationProxy = listOperationProxy;
    }

    public Boolean getPointageCategoriesOpe() {
        return pointageCategoriesOpe;
    }

    public void setPointageCategoriesOpe(Boolean pointageCategoriesOpe) {
        this.pointageCategoriesOpe = pointageCategoriesOpe;
    }

    public String getProductInformationSheet() {
        return productInformationSheet;
    }

    public void setProductInformationSheet(String productInformationSheet) {
        this.productInformationSheet = productInformationSheet;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}
