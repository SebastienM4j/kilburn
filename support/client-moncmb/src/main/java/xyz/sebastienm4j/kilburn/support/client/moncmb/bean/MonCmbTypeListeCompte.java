package xyz.sebastienm4j.kilburn.support.client.moncmb.bean;

public class MonCmbTypeListeCompte
{
    private String typeListeCompte;


    public String getTypeListeCompte()
    {
        return typeListeCompte;
    }

    public void setTypeListeCompte(String typeListeCompte)
    {
        this.typeListeCompte = typeListeCompte;
    }

}
