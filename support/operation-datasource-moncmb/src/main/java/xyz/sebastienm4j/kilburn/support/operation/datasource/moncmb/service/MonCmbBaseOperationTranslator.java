package xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.Assert;
import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationTranslationException;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationTranslator;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Mode;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Provenance;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Type;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;
import xyz.sebastienm4j.kilburn.api.operation.domain.factory.OperationDomainFactory;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.MonCmbBaseOperation;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Classe de base pour la traduction d'opération source de mon.cmb.fr.
 *
 * @param <DS> Type de la source de donnés
 * @param <O> Type de l'opération MonCmb
 */
public class MonCmbBaseOperationTranslator<DS, O extends MonCmbBaseOperation> implements OperationTranslator
{
	private static final DateTimeFormatter DATE_VALEUR_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yy");

	private Class<DS> dataSourceType;
	private Class<O> monCmbOperationType;

    private ObjectMapper objectMapper;
    private OperationDomainFactory operationDomainFactory;


    public MonCmbBaseOperationTranslator(Class<DS> dataSourceType, Class<O> monCmbOperationType, ObjectMapper objectMapper, OperationDomainFactory operationDomainFactory)
    {
        this.dataSourceType = dataSourceType;
        this.monCmbOperationType = monCmbOperationType;
        this.objectMapper = objectMapper;
        this.operationDomainFactory = operationDomainFactory;
    }


    @Override
    public boolean isApplicableTo(OperationSource operation)
    {
        Assert.notNull(operation, "Absence de l'opération");

        return operation.getDatasource().equals(dataSourceType.getName());
    }

    @Override
    public Operation translate(OperationSource operation, Set<Compte> comptes) throws OperationTranslationException
    {
        Assert.notNull(operation, "Absence de l'opération");
        Assert.notNull(comptes, "Absence de la liste des comptes");

        // Préparation

        Compte compte = comptes.stream()
                .filter(c -> c.getNumero().equals(operation.getCompte()))
                .findFirst()
                .orElseThrow(() -> new OperationTranslationException("Echec de traduction de l'opération source ["+operation.getId()+"], compte ["+operation.getCompte()+"] non trouvé (data : ["+operation.getData()+"])"));


        O operationData = null;
        try {
            operationData = objectMapper.readValue(operation.getData(), monCmbOperationType);

        } catch(IOException ex) {
            throw new OperationTranslationException("Echec de traduction de l'opération source ["+operation.getId()+"], problème de déserialisation (data : ["+operation.getData()+"])", ex);
        }

        // Traduction

        Operation op = operationDomainFactory.newOperation();

        op.setCompte(compte);
        op.setDate(LocalDate.parse(operationData.getDateValeur(), DATE_VALEUR_FORMATTER).atStartOfDay(ZoneId.of("CET")).toInstant());
        op.setDateValeur(null);
        op.setMode(translateMode(operationData));
        op.setTiers(null);
        op.setLibelle(operationData.getLibelleCourt());
        op.setReference(translateReference(operationData, op.getMode()));
        op.setMontant(BigDecimal.valueOf(operationData.getMontantEnEuro()));

        op.setCommentaire(operationData.getLibelleLong());
        op.setTags(Arrays.asList("Import", "mon.cmb.fr").stream().collect(Collectors.toSet()));
        op.setPointee(false);

        op.setProvenance(Provenance.IMPORT);
        op.setType(Type.UNITAIRE);

        op.setSources(Arrays.asList(operation));

        return op;
    }

    private Mode translateMode(O operationData)
    {
        String[] tokens = operationData.getLibelleCourt().split(" ");
        String mode = tokens[0].toUpperCase();

        switch(mode)
        {
            case "CARTE":
            case "ANN":
                return Mode.CARTE;

            case "RET":
                return Mode.RETRAIT;

            case "CHQ":
                return Mode.CHEQUE;

            case "REM":
                return Mode.REMISE;

            case "VIR":
                return Mode.VIREMENT;

            case "PRLV":
            case "ECH":
                return Mode.PRELEVEMENT;

            default:
                return null;
        }
    }

    private String translateReference(O operationData, Mode mode)
    {
        if(Mode.CHEQUE.equals(mode))
        {
            String[] tokens = operationData.getLibelleCourt().split(" ");
            return tokens[1];
        }

        return null;
    }

}
