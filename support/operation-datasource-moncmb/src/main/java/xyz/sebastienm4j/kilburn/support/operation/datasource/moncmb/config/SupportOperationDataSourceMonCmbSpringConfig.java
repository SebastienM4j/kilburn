package xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import xyz.sebastienm4j.kilburn.api.operation.domain.factory.OperationDomainFactory;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.MonCmbOperation;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.MonCmbOperationEpargneProxy;
import xyz.sebastienm4j.kilburn.support.client.moncmb.config.SupportClientMonCmbSpringConfig;
import xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.service.MonCmbBaseOperationTranslator;
import xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.service.MonCmbEpargneOperationDataSource;
import xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.service.MonCmbOperationDataSource;

/**
 * Configuration Spring pour la source de données d'opérations
 * en provenant du site mon.cmb.fr.
 */
@Configuration
@ComponentScan("xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.service")
@Import(SupportClientMonCmbSpringConfig.class)
public class SupportOperationDataSourceMonCmbSpringConfig
{
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private OperationDomainFactory operationDomainFactory;


    @Bean
    public MonCmbBaseOperationTranslator<MonCmbOperationDataSource, MonCmbOperation> monCmbOperationTranslator()
    {
        return new MonCmbBaseOperationTranslator<>(MonCmbOperationDataSource.class,
                                                    MonCmbOperation.class,
                                                    objectMapper,
                                                    operationDomainFactory);
    }

    @Bean
    public MonCmbBaseOperationTranslator<MonCmbEpargneOperationDataSource, MonCmbOperationEpargneProxy> monCmbEpargneOperationTranslator()
    {
        return new MonCmbBaseOperationTranslator<>(MonCmbEpargneOperationDataSource.class,
                                                    MonCmbOperationEpargneProxy.class,
                                                    objectMapper,
                                                    operationDomainFactory);
    }
}
