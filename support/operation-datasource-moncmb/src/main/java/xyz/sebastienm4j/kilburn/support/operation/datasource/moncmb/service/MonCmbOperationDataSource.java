package xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationDataSource;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationDataSourceException;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;
import xyz.sebastienm4j.kilburn.api.operation.domain.factory.OperationDomainFactory;
import xyz.sebastienm4j.kilburn.shared.rest.client.RestClientUnexpectedResponseException;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.*;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.MonCmbDetailComptePayload.FiltreOperations;
import xyz.sebastienm4j.kilburn.support.client.moncmb.exception.MonCmbAuthenticationException;
import xyz.sebastienm4j.kilburn.support.client.moncmb.service.MonCmbAuthenticationService;
import xyz.sebastienm4j.kilburn.support.client.moncmb.service.MonCmbDomiApiClientService;
import xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.config.DataSourceProperties;
import xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.config.DataSourceProperties.Accounts;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static org.springframework.util.CollectionUtils.isEmpty;
import static org.springframework.util.StringUtils.hasText;

/**
 * Import des opérations en attaquant les APIs REST du site mon.cmb.fr.
 */
@Service
public class MonCmbOperationDataSource implements OperationDataSource
{
    private static final Logger logger = LoggerFactory.getLogger(MonCmbOperationDataSource.class);


    @Autowired
    @Qualifier("monCmbAuthenticationApiService")
    private MonCmbAuthenticationService monCmbAuthenticationService;

    @Autowired
    private MonCmbDomiApiClientService monCmbDomiApiClientService;

    @Autowired
    private OperationDomainFactory operationDomainFactory;

    @Autowired
    private ObjectMapper objectMapper;


    @Override
    public Collection<OperationSource> getLastOperations(Map<String, String> dataSourceProperties) throws OperationDataSourceException
    {
        return getLastOperations(new DataSourceProperties(dataSourceProperties));
    }


    /*
     * Récupération des opérations pour un accès paramétré
     */

    private Collection<OperationSource> getLastOperations(DataSourceProperties properties)
    {
        Collection<OperationSource> operations = new ArrayList<>();
        MonCmbAuthToken token;

        try
        {
            token = monCmbAuthenticationService.authenticate(properties.getCredentials().getLogin(), properties.getCredentials().getPassword());

        } catch(MonCmbAuthenticationException ex) {
            logger.error("Problème d'authentification sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
            return operations;
        }

        try
        {
            MonCmbTypeListeCompte typeListeCompte = new MonCmbTypeListeCompte();
            typeListeCompte.setTypeListeCompte("COMPTE_SOLDE_COMPTES_CHEQUES");
            MonCmbSyntheseComptes syntheseComptes = monCmbDomiApiClientService.syntheseComptes(token, typeListeCompte);

            List<MonCmbCompteTitulaireCotitulaire> comptes = syntheseComptes.getListCompteTitulaireCotitulaire();
            if(!isEmpty(comptes)) {
                logger.debug("[{}] compte(s) (co)titulaire trouvé(s) sur mon.cmb.fr pour l'accès [{}/******]", comptes.size(), properties.getCredentials().getLogin());
                comptes.forEach(cpt -> operations.addAll(getLastOperations(properties, token, cpt)));
            }

        } catch(RestClientUnexpectedResponseException ex) {
            logger.error("Problème de récupération de la synthèse des comptes sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
        }

        return operations;
    }

    /*
     * Récupération des opérations pour un compte
     */

    private Collection<OperationSource> getLastOperations(DataSourceProperties properties, MonCmbAuthToken token, MonCmbCompteTitulaireCotitulaire compte)
    {
        String numeroCompte = compte.getIdentifiantTechnique().trim();
        Predicate<String> matcher = num -> num.equals(numeroCompte);

        if(!isInclude(properties, matcher) || isExclude(properties, matcher)) {
            logger.debug("Le compte [{}] sur mon.cmb.fr pour l'accès [{}/******] est exclu", numeroCompte, properties.getCredentials().getLogin());
            return new ArrayList<>();
        }

        MonCmbDetailComptePayload filtre = new MonCmbDetailComptePayload();
        filtre.setFiltreOperationsComptabilisees(properties.getPeriod() != null ? properties.getPeriod() : FiltreOperations.SIX_DERNIERES_SEMAINES);
        filtre.setIndex(compte.getIndex());
        filtre.setCompteCheque(compte.getLib().contains("COMPTE CHEQUES"));
        switch(filtre.getFiltreOperationsComptabilisees())
        {
            case CINQ_DERNIERES_OPERATIONS:
                filtre.setNonBloquant(true);
                break;

            default:
                filtre.setCallEtalis(true);
                filtre.setPrefAccount(false);
                break;
        }

        try
        {
            MonCmbDetailCompte detailCompte = monCmbDomiApiClientService.detailCompte(token, filtre);

            if(isEmpty(detailCompte.getListOperationProxy())) {
                logger.info("Aucune opération récupérée pour le compte [{}] sur mon.cmb.fr pour l'accès [{}/******]", numeroCompte, properties.getCredentials().getLogin());
                return new ArrayList<>();
            }

            Collection<OperationSource> operations = new ArrayList<>();
            for(MonCmbOperationProxy op : detailCompte.getListOperationProxy())
            {
                // Pour une même opération, ces valeurs peuvent changer.
                // Afin d'éviter un doublon, on force leur veleur à false.
                op.setIndicateurTempsReel(false);
                op.setMarquage(false);
                // Palliatifs à la disparition du libellé long s'il est identique
                // au libellé court ou au fait que le libellé court n'est plus contenu
                // dans le libellé long
                if(!hasText(op.getLibelleLong())) {
                    op.setLibelleLong(op.getLibelleCourt());
                } else if(!op.getLibelleLong().startsWith(op.getLibelleCourt())) {
                    op.setLibelleLong(op.getLibelleCourt()+op.getLibelleLong());
                }

                OperationSource os = operationDomainFactory.newOperationSource();
                os.setCompte(numeroCompte);
                os.setDatasource(getClass().getName());
                os.setData(objectMapper.writeValueAsString(op));
                os.setDateImport(Instant.now());
                operations.add(os);
            }

            logger.info("[{}] opération(s) récupérée(s) pour le compte [{}] sur mon.cmb.fr pour l'accès [{}/******]", operations.size(), numeroCompte, properties.getCredentials().getLogin());
            return operations;

        } catch(RestClientUnexpectedResponseException ex) {
            logger.error("Problème de récupération du détail du compte ["+numeroCompte+"] sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
        } catch(JsonProcessingException ex) {
            logger.error("Problème de sérialisation des opérations du compte ["+numeroCompte+"] sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
        }

        return new ArrayList<>();
    }

    private Collection<OperationSource> getLastOperations(DataSourceProperties properties, MonCmbAuthToken token, MonCmbSavingsAccount compte)
    {
        String numeroCompte = compte.getIndex().trim();
        Predicate<String> matcher = num -> num.equals(numeroCompte);

        if(!isInclude(properties, matcher) || isExclude(properties, matcher)) {
            logger.debug("Le compte d'épargne [{}] sur mon.cmb.fr pour l'accès [{}/******] est exclu", numeroCompte, properties.getCredentials().getLogin());
            return new ArrayList<>();
        }


        MonCmbDetailCompteEpargnePayload payload = new MonCmbDetailCompteEpargnePayload();
        payload.setCallEtalis(false);
        payload.setDomirama(true);
        payload.setIndex(numeroCompte);

        try
        {
            MonCmbDetailCompteEpargne detailCompte = monCmbDomiApiClientService.detailCompteEpargne(token, payload);

            if(isEmpty(detailCompte.getListOperationProxy())) {
                logger.info("Aucune opération récupérée pour le compte d'épargne [{}] sur mon.cmb.fr pour l'accès [{}/******]", numeroCompte, properties.getCredentials().getLogin());
                return new ArrayList<>();
            }

            Collection<OperationSource> operations = new ArrayList<>();
            for(MonCmbOperationEpargneProxy op : detailCompte.getListOperationProxy())
            {
                // Pour une même opération, ces valeurs peuvent changer.
                // Afin d'éviter un doublon, on force leur veleur.
                op.setMarquage(false);
                op.setOperationId(null);

                OperationSource os = operationDomainFactory.newOperationSource();
                os.setCompte(numeroCompte);
                os.setDatasource(getClass().getName());
                os.setData(objectMapper.writeValueAsString(op));
                os.setDateImport(Instant.now());
                operations.add(os);
            }

            logger.info("[{}] opération(s) récupérée(s) pour le compte d'épargne [{}] sur mon.cmb.fr pour l'accès [{}/******]", operations.size(), numeroCompte, properties.getCredentials().getLogin());
            return operations;

        } catch(RestClientUnexpectedResponseException ex) {
            logger.error("Problème de récupération du détail du compte d'épargne ["+numeroCompte+"] sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
        } catch(JsonProcessingException ex) {
            logger.error("Problème de sérialisation des opérations du compte d'épargne ["+numeroCompte+"] sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
        }

        return new ArrayList<>();
    }

    /*
     * Inclusion / exclusion d'un compte
     */

    private boolean isInclude(DataSourceProperties properties, Predicate<String> matcher)
    {
        Accounts accounts = properties.getAccounts();
        if(accounts == null || isEmpty(accounts.getInclude())) {
            return true;
        }
        return accounts.getInclude().stream().anyMatch(matcher);
    }

    private boolean isExclude(DataSourceProperties properties, Predicate<String> matcher)
    {
        Accounts accounts = properties.getAccounts();
        if(accounts == null || isEmpty(accounts.getExclude())) {
            return false;
        }
        return accounts.getExclude().stream().anyMatch(matcher);
    }

}
