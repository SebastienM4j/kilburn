package xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.config;

import org.springframework.util.Assert;

import java.util.Map;

public class EpargneDataSourceProperties
{
    public static class Credentials
    {
        private String login;
        private String password;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class Account
    {
        public enum Horizon { SHORT, MEDIUM, LONG }

        private Horizon horizon;
        private String libelleContrat;
        private String nomTitulaire;
        private String numero;

        public Horizon getHorizon() {
            return horizon;
        }

        public void setHorizon(Horizon horizon) {
            this.horizon = horizon;
        }

        public String getLibelleContrat() {
            return libelleContrat;
        }

        public void setLibelleContrat(String libelleContrat) {
            this.libelleContrat = libelleContrat;
        }

        public String getNomTitulaire() {
            return nomTitulaire;
        }

        public void setNomTitulaire(String nomTitulaire) {
            this.nomTitulaire = nomTitulaire;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }
    }


    private Credentials credentials;
    private Account account;


    public EpargneDataSourceProperties(Map<String, String> properties)
    {
        Assert.notEmpty(properties, "Propriétés absentes ou vides");

        String login = properties.get("login");
        String password = properties.get("password");

        Assert.hasText(login, "Identifiant absent ou vide");
        Assert.hasText(password, "Mot de passe absent ou vide");

        this.credentials = new Credentials();
        this.credentials.setLogin(login);
        this.credentials.setPassword(password);

        String horizon = properties.get("horizon");
        String libelleContrat = properties.getOrDefault("libelle-contrat", properties.get("libelleContrat"));
        String nomTitulaire = properties.getOrDefault("nom-titulaire", properties.get("nomTitulaire"));
        String numero = properties.get("numero");

        Assert.hasText(horizon, "Horizon absent ou vide");
        Assert.hasText(libelleContrat, "Libellé du contrat absent ou vide");
        Assert.hasText(nomTitulaire, "Nom du titulaire absent ou vide");
        Assert.hasText(numero, "Numéro de compte absent ou vide");

        this.account = new Account();
        this.account.setHorizon(Account.Horizon.valueOf(horizon));
        this.account.setLibelleContrat(libelleContrat);
        this.account.setNomTitulaire(nomTitulaire);
        this.account.setNumero(numero);
    }


    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

}
