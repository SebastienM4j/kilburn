package xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationDataSource;
import xyz.sebastienm4j.kilburn.api.operation.datasource.OperationDataSourceException;
import xyz.sebastienm4j.kilburn.api.operation.domain.OperationSource;
import xyz.sebastienm4j.kilburn.api.operation.domain.factory.OperationDomainFactory;
import xyz.sebastienm4j.kilburn.shared.rest.client.RestClientUnexpectedResponseException;
import xyz.sebastienm4j.kilburn.support.client.moncmb.bean.*;
import xyz.sebastienm4j.kilburn.support.client.moncmb.exception.MonCmbAuthenticationException;
import xyz.sebastienm4j.kilburn.support.client.moncmb.service.MonCmbAuthenticationService;
import xyz.sebastienm4j.kilburn.support.client.moncmb.service.MonCmbDomiApiClientService;
import xyz.sebastienm4j.kilburn.support.operation.datasource.moncmb.config.EpargneDataSourceProperties;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Import des opérations (des comptes d'épargne) en attaquant les APIs REST du site mon.cmb.fr.
 */
@Service
public class MonCmbEpargneOperationDataSource implements OperationDataSource
{
    private static final Logger logger = LoggerFactory.getLogger(MonCmbEpargneOperationDataSource.class);


    @Autowired
    @Qualifier("monCmbAuthenticationApiService")
    private MonCmbAuthenticationService monCmbAuthenticationService;

    @Autowired
    private MonCmbDomiApiClientService monCmbDomiApiClientService;

    @Autowired
    private OperationDomainFactory operationDomainFactory;

    @Autowired
    private ObjectMapper objectMapper;


    @Override
    public Collection<OperationSource> getLastOperations(Map<String, String> dataSourceProperties) throws OperationDataSourceException
    {
        return getLastOperations(new EpargneDataSourceProperties(dataSourceProperties));
    }


    /*
     * Récupération des opérations pour un accès paramétré
     */

    private Collection<OperationSource> getLastOperations(EpargneDataSourceProperties properties)
    {
        Collection<OperationSource> operations = new ArrayList<>();
        MonCmbAuthToken token;

        try
        {
            token = monCmbAuthenticationService.authenticate(properties.getCredentials().getLogin(), properties.getCredentials().getPassword());

        } catch(MonCmbAuthenticationException ex) {
            logger.error("Problème d'authentification sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
            return operations;
        }

        try
        {
            MonCmbSyntheseEpargne syntheseEpargne = monCmbDomiApiClientService.syntheseComptesEpargne(token);

            List<MonCmbHorizon> horizons = syntheseEpargne.getHorizons();
            if(!isEmpty(horizons))
            {
                MonCmbHorizon horizon = horizons.stream()
                                                .filter(h -> h.getType().equals(properties.getAccount().getHorizon().name()))
                                                .findFirst()
                                                .orElse(null);
                if(horizon == null) {
                    return operations;
                }

                List<MonCmbSavingsProduct> products = horizon.getSavingsProducts();
                if(isEmpty(products)) {
                    return operations;
                }

                for(MonCmbSavingsProduct p : products)
                {
                    List<MonCmbSavingsAccount> accounts = p.getSavingsAccounts();
                    if(!isEmpty(accounts))
                    {
                        MonCmbSavingsAccount account = accounts.stream()
                                                               .filter(sa ->
                                                                   properties.getAccount().getLibelleContrat().equals(sa.getLibelleContrat()) &&
                                                                   properties.getAccount().getNomTitulaire().equals(sa.getNomTitulaire())
                                                               )
                                                               .findFirst()
                                                               .orElse(null);

                        if(account != null)
                        {
                            logger.debug("Compte d'épargne [{}/{}] trouvé sur mon.cmb.fr pour l'accès [{}/******]", properties.getAccount().getLibelleContrat(), properties.getAccount().getNomTitulaire(), properties.getCredentials().getLogin());

                            operations.addAll(getLastOperations(properties, token, account));
                            break;
                        }
                    }
                }
            }

        } catch(RestClientUnexpectedResponseException ex) {
            logger.error("Problème de récupération de la synthèse des comptes d'épargne sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
        }

        return operations;
    }

    /*
     * Récupération des opérations pour un compte
     */

    private Collection<OperationSource> getLastOperations(EpargneDataSourceProperties properties, MonCmbAuthToken token, MonCmbSavingsAccount compte)
    {
        MonCmbDetailCompteEpargnePayload payload = new MonCmbDetailCompteEpargnePayload();
        payload.setCallEtalis(false);
        payload.setDomirama(true);
        payload.setIndex(compte.getIndex().trim());

        try
        {
            MonCmbDetailCompteEpargne detailCompte = monCmbDomiApiClientService.detailCompteEpargne(token, payload);

            if(isEmpty(detailCompte.getListOperationProxy())) {
                logger.info("Aucune opération récupérée pour le compte d'épargne [{}/{}] sur mon.cmb.fr pour l'accès [{}/******]", properties.getAccount().getLibelleContrat(), properties.getAccount().getNomTitulaire(), properties.getCredentials().getLogin());
                return new ArrayList<>();
            }

            Collection<OperationSource> operations = new ArrayList<>();
            for(MonCmbOperationEpargneProxy op : detailCompte.getListOperationProxy())
            {
                // Pour une même opération, ces valeurs peuvent changer.
                // Afin d'éviter un doublon, on force leur veleur.
                op.setMarquage(false);
                op.setOperationId(null);

                OperationSource os = operationDomainFactory.newOperationSource();
                os.setCompte(properties.getAccount().getNumero());
                os.setDatasource(getClass().getName());
                os.setData(objectMapper.writeValueAsString(op));
                os.setDateImport(Instant.now());
                operations.add(os);
            }

            logger.info("[{}] opération(s) récupérée(s) pour le compte d'épargne [{}/{}] sur mon.cmb.fr pour l'accès [{}/******]", operations.size(), properties.getAccount().getLibelleContrat(), properties.getAccount().getNomTitulaire(), properties.getCredentials().getLogin());
            return operations;

        } catch(RestClientUnexpectedResponseException ex) {
            logger.error("Problème de récupération du détail du compte d'épargne ["+properties.getAccount().getLibelleContrat()+"/"+properties.getAccount().getNomTitulaire()+"] sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
        } catch(JsonProcessingException ex) {
            logger.error("Problème de sérialisation des opérations du compte d'épargne ["+properties.getAccount().getLibelleContrat()+"/"+properties.getAccount().getNomTitulaire()+"] sur mon.cmb.fr pour l'accès ["+properties.getCredentials().getLogin()+"/******]", ex);
        }

        return new ArrayList<>();
    }

}
