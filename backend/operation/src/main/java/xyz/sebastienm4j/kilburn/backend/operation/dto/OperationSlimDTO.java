package xyz.sebastienm4j.kilburn.backend.operation.dto;

import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Mode;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Provenance;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Type;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Set;

/**
 * DTO pour transporter une opération bancaire
 * sans les sources, ni le compte.
 */
public class OperationSlimDTO
{
    private Long id;

    private Instant date;
    private Instant dateValeur;
    private Mode mode;
    private String tiers;
    private String libelle;
    private String reference;
    private BigDecimal montant;

    private String commentaire;
    private Set<String> tags;
    private boolean pointee;

    private Provenance provenance;
    private Type type;



    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Instant getDate()
    {
        return date;
    }

    public void setDate(Instant date)
    {
        this.date = date;
    }

    public Instant getDateValeur()
    {
        return dateValeur;
    }

    public void setDateValeur(Instant dateValeur)
    {
        this.dateValeur = dateValeur;
    }

    public Mode getMode()
    {
        return mode;
    }

    public void setMode(Mode mode)
    {
        this.mode = mode;
    }

    public String getTiers()
    {
        return tiers;
    }

    public void setTiers(String tiers)
    {
        this.tiers = tiers;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public String getReference()
    {
        return reference;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public BigDecimal getMontant()
    {
        return montant;
    }

    public void setMontant(BigDecimal montant)
    {
        this.montant = montant;
    }

    public String getCommentaire()
    {
        return commentaire;
    }

    public void setCommentaire(String commentaire)
    {
        this.commentaire = commentaire;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public boolean isPointee()
    {
        return pointee;
    }

    public void setPointee(boolean pointee)
    {
        this.pointee = pointee;
    }

    public Provenance getProvenance()
    {
        return provenance;
    }

    public void setProvenance(Provenance provenance)
    {
        this.provenance = provenance;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

}
