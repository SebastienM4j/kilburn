package xyz.sebastienm4j.kilburn.backend.operation.resource;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Provenance;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation.Type;
import xyz.sebastienm4j.kilburn.backend.operation.dto.OperationPointageDTO;
import xyz.sebastienm4j.kilburn.backend.operation.dto.OperationSlimDTO;
import xyz.sebastienm4j.kilburn.core.compte.domain.entity.CompteEntity;
import xyz.sebastienm4j.kilburn.core.compte.domain.repository.CompteRepository;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationRepository;
import xyz.sebastienm4j.kilburn.shared.rest.exception.NotFoundException;
import xyz.sebastienm4j.kilburn.shared.rest.exception.UnprocessableEntityException;

import java.util.List;

@RestController
@RequestMapping("/rest/backend/comptes/{idCompte}/operations")
public class OperationResource
{
    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    @Qualifier("backendOperationDTOMapper")
    private MapperFacade mapper;


    @GetMapping
    public List<OperationSlimDTO> findAllOrderByDateDesc(@PathVariable Integer idCompte)
    {
        List<OperationEntity> operations = operationRepository.findByCompteIdOrderByDateDesc(idCompte);
        return mapper.mapAsList(operations, OperationSlimDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addOperation(@PathVariable Integer idCompte, @RequestBody OperationSlimDTO operationDTO)
    {
        if(operationDTO.getId() != null) {
            throw new UnprocessableEntityException();
        }

        CompteEntity compte = compteRepository.findOne(idCompte);
        if(compte == null) {
            throw new NotFoundException();
        }

        OperationEntity operation = mapper.map(operationDTO, OperationEntity.class);
        operation.setCompte(compte);
        operation.setProvenance(Provenance.MANUELLE);
        operation.setType(Type.UNITAIRE);

        operationRepository.save(operation);
    }

    @PutMapping("/{id}")
    public void updateOperation(@PathVariable Integer idCompte, @PathVariable Long id, @RequestBody OperationSlimDTO operationDTO)
    {
        if(operationDTO.getId() == null || !operationDTO.getId().equals(id)) {
            throw new UnprocessableEntityException();
        }

        OperationEntity existing = operationRepository.findByCompteIdAndId(idCompte, id);
        if(existing == null) {
            throw new NotFoundException();
        }

        OperationEntity operation = mapper.map(operationDTO, OperationEntity.class);
        operation.setCompte(existing.getCompte());
        operation.setSources(existing.getSources());

        operationRepository.save(operation);
    }

    @DeleteMapping("/{id}")
    public void deleteOperation(@PathVariable Integer idCompte, @PathVariable Long id)
    {
        OperationEntity existing = operationRepository.findByCompteIdAndId(idCompte, id);
        if(existing == null) {
            throw new NotFoundException();
        }

        operationRepository.delete(id);
    }

    @PatchMapping("/{id}/pointage")
    public void pointageOperation(@PathVariable Integer idCompte, @PathVariable Long id, @RequestBody OperationPointageDTO pointage)
    {
        OperationEntity operation = operationRepository.findByCompteIdAndId(idCompte, id);
        if(operation == null) {
            throw new NotFoundException();
        }

        operation.setPointee(pointage.isPointee());
        operationRepository.save(operation);
    }

}
