package xyz.sebastienm4j.kilburn.backend.operation.dto;

/**
 * DTO pour transporter une règle d'opération
 * pour les besoins d'affichage dans une liste
 * basique.
 */
public class OperationRegleListDTO
{
    private Integer id;
    private String libelle;
    private boolean auto;
    private Integer ordre;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public boolean isAuto() {
        return auto;
    }

    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

}
