package xyz.sebastienm4j.kilburn.backend.operation.resource;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import xyz.sebastienm4j.kilburn.api.operation.domain.Operation;
import xyz.sebastienm4j.kilburn.api.operation.domain.regle.OperationRegleRegistry;
import xyz.sebastienm4j.kilburn.backend.operation.dto.OperationRegleDTO;
import xyz.sebastienm4j.kilburn.backend.operation.dto.OperationRegleListDTO;
import xyz.sebastienm4j.kilburn.backend.operation.dto.OperationRegleOrdreDTO;
import xyz.sebastienm4j.kilburn.core.operation.domain.bean.ScriptValidation;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.entity.OperationRegleEntity;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationRegleRepository;
import xyz.sebastienm4j.kilburn.core.operation.domain.repository.OperationRepository;
import xyz.sebastienm4j.kilburn.core.operation.domain.service.OperationRegleService;
import xyz.sebastienm4j.kilburn.shared.rest.exception.NotFoundException;
import xyz.sebastienm4j.kilburn.shared.rest.exception.UnprocessableEntityException;

import javax.script.ScriptException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

@RestController
@RequestMapping("/rest/backend/regles/operation")
public class OperationRegleResource
{
    @Autowired
    private OperationRegleRepository operationRegleRepository;

    @Autowired
    private OperationRegleService operationRegleService;

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    @Qualifier("backendOperationDTOMapper")
    private MapperFacade mapper;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OperationRegleDTO addOperationRegle(@RequestBody OperationRegleDTO operationRegleDTO)
    {
        if(operationRegleDTO.getId() != null) {
            throw new UnprocessableEntityException();
        }
        if(!OperationRegleRegistry.hasType(operationRegleDTO.getRegle())) {
            throw new UnprocessableEntityException();
        }

        ScriptValidation validation = operationRegleService.checkScript(OperationRegleRegistry.getByType(operationRegleDTO.getRegle()), operationRegleDTO.getLangage(), operationRegleDTO.getScript());
        if(!validation.isValid()) {
            throw new UnprocessableEntityException();
        }

        OperationRegleEntity regle = mapper.map(operationRegleDTO, OperationRegleEntity.class);
        Long count = operationRegleRepository.countByRegle(regle.getRegle());
        regle.setOrdre(count.intValue()+1);

        OperationRegleEntity saved = operationRegleRepository.save(regle);

        return mapper.map(saved, OperationRegleDTO.class);
    }

    @GetMapping("/{id}")
    public OperationRegleDTO getOperationRegle(@PathVariable Integer id)
    {
        OperationRegleEntity regle = operationRegleRepository.findOne(id);
        if(regle == null) {
            throw new NotFoundException();
        }

        return mapper.map(regle, OperationRegleDTO.class);
    }

    @PutMapping("/{id}")
    public void updateOperationRegle(@PathVariable Integer id, @RequestBody OperationRegleDTO operationRegleDTO)
    {
        if(operationRegleDTO.getId() == null || !operationRegleDTO.getId().equals(id)) {
            throw new UnprocessableEntityException();
        }

        if(!operationRegleRepository.exists(id)) {
            throw new NotFoundException();
        }

        ScriptValidation validation = operationRegleService.checkScript(OperationRegleRegistry.getByType(operationRegleDTO.getRegle()), operationRegleDTO.getLangage(), operationRegleDTO.getScript());
        if(!validation.isValid()) {
            throw new UnprocessableEntityException();
        }

        OperationRegleEntity regle = mapper.map(operationRegleDTO, OperationRegleEntity.class);

        operationRegleRepository.save(regle);
    }

    @DeleteMapping("/{id}")
    public void deleteOperationRegle(@PathVariable Integer id)
    {
        if(!operationRegleRepository.exists(id)) {
            throw new NotFoundException();
        }

        operationRegleRepository.delete(id);
    }

    @PatchMapping("/{id}/ordre")
    public void reorderOperationRegle(@PathVariable Integer id, @RequestBody OperationRegleOrdreDTO body)
    {
        OperationRegleEntity regle = operationRegleRepository.findOne(id);
        if(regle == null) {
            throw new NotFoundException();
        }

        operationRegleService.reorderRegle(regle, body.getPosition());
    }

    @PostMapping("/{id}/applications")
    public void applyOperationRegle(@PathVariable Integer id, @RequestBody List<Long> operationIds) throws ScriptException
    {
        OperationRegleEntity regle = operationRegleRepository.findOne(id);
        if(regle == null) {
            throw new NotFoundException();
        }

        List<OperationEntity> operations = operationRepository.findByIdIn(operationIds);
        if(isEmpty(operations)) {
            throw new NotFoundException();
        }

        operationRegleService.applyRegleAndSave(regle, operations.stream().map(o -> (Operation)o).collect(Collectors.toList()));
    }


    @PostMapping("/script-validations")
    public ScriptValidation checkScript(@RequestBody OperationRegleDTO operationRegleDTO)
    {
        if(!OperationRegleRegistry.hasType(operationRegleDTO.getRegle()) ||
           operationRegleDTO.getLangage() == null ||
           !StringUtils.hasText(operationRegleDTO.getScript()))
        {
            throw new UnprocessableEntityException();
        }

        return operationRegleService.checkScript(OperationRegleRegistry.getByType(operationRegleDTO.getRegle()), operationRegleDTO.getLangage(), operationRegleDTO.getScript());
    }


    @GetMapping("/types")
    public List<String> getOperationRegleTypes()
    {
        return OperationRegleRegistry.regleTypes().stream()
                                                  .map(Class::getName)
                                                  .collect(Collectors.toList());
    }

    @GetMapping("/types/{type:.*}")
    public List<?> findAllByType(@PathVariable String type, @RequestParam(name="full", defaultValue="false") boolean full)
    {
        List<OperationRegleEntity> regles = operationRegleRepository.findByRegleOrderByOrdreAsc(type);

        if(!full) {
            return mapper.mapAsList(regles, OperationRegleListDTO.class);
        } else {
            return mapper.mapAsList(regles, OperationRegleDTO.class);
        }
    }

}
