package xyz.sebastienm4j.kilburn.backend.operation.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import xyz.sebastienm4j.kilburn.core.operation.domain.config.CoreOperationDomainSpringConfig;
import xyz.sebastienm4j.kilburn.shared.rest.config.SharedRestStringConfig;

@Configuration
@Import({
	CoreOperationDomainSpringConfig.class,
	SharedRestStringConfig.class
})
@ComponentScan({
    "xyz.sebastienm4j.kilburn.backend.operation.mapping",
    "xyz.sebastienm4j.kilburn.backend.operation.resource"
})
public class BackendOperationSpringConfig
{

}
