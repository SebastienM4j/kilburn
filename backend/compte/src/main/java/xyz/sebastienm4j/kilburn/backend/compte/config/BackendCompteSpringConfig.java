package xyz.sebastienm4j.kilburn.backend.compte.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import xyz.sebastienm4j.kilburn.core.compte.domain.config.CoreCompteDomainSpringConfig;
import xyz.sebastienm4j.kilburn.shared.rest.config.SharedRestStringConfig;

@Configuration
@Import({
	CoreCompteDomainSpringConfig.class,
	SharedRestStringConfig.class
})
@ComponentScan({
    "xyz.sebastienm4j.kilburn.backend.compte.mapping",
    "xyz.sebastienm4j.kilburn.backend.compte.resource"
})
public class BackendCompteSpringConfig
{

}
