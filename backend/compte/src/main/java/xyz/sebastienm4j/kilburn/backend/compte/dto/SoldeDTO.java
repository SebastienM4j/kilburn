package xyz.sebastienm4j.kilburn.backend.compte.dto;

import java.math.BigDecimal;

/**
 * DTO pour transporter les soldes d'un compte.
 */
public class SoldeDTO
{
    private BigDecimal pointe;
    private BigDecimal avenir;
    private BigDecimal solde;


    public BigDecimal getPointe() {
        return pointe;
    }

    public void setPointe(BigDecimal pointe) {
        this.pointe = pointe;
    }

    public BigDecimal getAvenir() {
        return avenir;
    }

    public void setAvenir(BigDecimal avenir) {
        this.avenir = avenir;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

}
