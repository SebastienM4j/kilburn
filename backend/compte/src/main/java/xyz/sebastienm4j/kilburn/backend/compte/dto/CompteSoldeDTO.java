package xyz.sebastienm4j.kilburn.backend.compte.dto;

import xyz.sebastienm4j.kilburn.api.compte.domain.Compte.Type;

import java.math.BigDecimal;
import java.util.Set;

/**
 * DTO pour transporter un compte bancaire avec son solde.
 */
public class CompteSoldeDTO
{
    private Integer id;

    private Type type;
    private String libelle;

    private String numero;

    private Set<String> tags;

    private BigDecimal solde;


    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public String getNumero()
    {
        return numero;
    }

    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    public Set<String> getTags()
    {
        return tags;
    }

    public void setTags(Set<String> tags)
    {
        this.tags = tags;
    }

    public BigDecimal getSolde()
    {
        return solde;
    }

    public void setSolde(BigDecimal solde)
    {
        this.solde = solde;
    }

}
