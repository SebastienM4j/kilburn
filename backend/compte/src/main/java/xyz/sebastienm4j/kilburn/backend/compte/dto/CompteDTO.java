package xyz.sebastienm4j.kilburn.backend.compte.dto;

import xyz.sebastienm4j.kilburn.api.compte.domain.Compte;

import java.util.Set;

/**
 * DTO pour transporter un compte bancaire.
 */
public class CompteDTO implements Compte
{
    private Integer id;

    private Type type;
    private String libelle;

    private String numero;
    private String iban;

    private String ribBanque;
    private String ribGuichet;
    private String ribCompte;
    private String ribCle;

    private String bic;

    private Set<String> tags;


    @Override
    public Integer getId()
    {
        return id;
    }

    @Override
    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    public Type getType()
    {
        return type;
    }

    @Override
    public void setType(Type type)
    {
        this.type = type;
    }

    @Override
    public String getLibelle()
    {
        return libelle;
    }

    @Override
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    @Override
    public String getNumero()
    {
        return numero;
    }

    @Override
    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    @Override
    public String getIban()
    {
        return iban;
    }

    @Override
    public void setIban(String iban)
    {
        this.iban = iban;
    }

    @Override
    public String getRibBanque()
    {
        return ribBanque;
    }

    @Override
    public void setRibBanque(String ribBanque)
    {
        this.ribBanque = ribBanque;
    }

    @Override
    public String getRibGuichet()
    {
        return ribGuichet;
    }

    @Override
    public void setRibGuichet(String ribGuichet)
    {
        this.ribGuichet = ribGuichet;
    }

    @Override
    public String getRibCompte()
    {
        return ribCompte;
    }

    @Override
    public void setRibCompte(String ribCompte)
    {
        this.ribCompte = ribCompte;
    }

    @Override
    public String getRibCle()
    {
        return ribCle;
    }

    @Override
    public void setRibCle(String ribCle)
    {
        this.ribCle = ribCle;
    }

    @Override
    public String getBic()
    {
        return bic;
    }

    @Override
    public void setBic(String bic)
    {
        this.bic = bic;
    }

    @Override
    public Set<String> getTags()
    {
        return tags;
    }

    @Override
    public void setTags(Set<String> tags)
    {
        this.tags = tags;
    }

}
